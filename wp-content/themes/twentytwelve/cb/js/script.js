jQuery(function(){
    var img = jQuery(".event_note_image img");
    if(img.length){
        var width = img.width();
        var height = img.height();
        if(width > height){
            img.css("width", "350px");
        }
        else{
            img.css("height", "450px");
        }
    }
    
    jQuery(document).on("click", ".events-table tr", function(){
        var href = jQuery(jQuery(this).find("a")).attr("href");
        location.href = href;
    });
    
    jQuery(document).on("submit", "#event-form", function(e){
//        e.preventDefault();
        jQuery(this).find("input[type='submit']").attr("disabled", "disabled");
        jQuery(this).find(".eventloading").css("display", "block");
    });
    
   jQuery(document).on("keyup", "input.ticket_price", function(){
       var value = parseInt(jQuery(this).val());
       if(value.toString() != "NaN"){
           value = value + 1;
           var label = jQuery(this).parent().find("#label-commission");
           var real = jQuery(this).parent().find("#price-commission");
           if(label.length && real.length){
               jQuery(label).html("Ticket cost with commission will be £"+value);
               real.val(value);
           }
       }
       else{
          var label = jQuery(this).parent().find("#label-commission");
          jQuery(label).html("");
       }
   });
   
   jQuery(document).on("click", ".sendemails", function(e){
      e.stopPropagation();
       var eventid = jQuery(this).attr("id").replace("sendemail-","");
       
       jQuery("#em-sendemails").dialog({
            autoOpen: false,
            height: 300,
            width: 550,
            modal: true,
            buttons: {
                "Send": function(){
                    var subject = jQuery("#em-sendemails").find("#subject").val();
                    var content = jQuery("#em-sendemails").find("#content").val();
                    var data = {
                        'action': 'send_emails',
                        'eventid': eventid,
                        'subject': subject,
                        'content': content
                    };

                    jQuery.post(ajaxurl, data, function(response) {
                            alert(response.replace("0",""));
                    });
                },
                Cancel: function() {
                    jQuery("#em-sendemails").dialog( "close" );
                }
            },
            close: function() {
                
            }
        });
        jQuery("#em-sendemails").dialog('open');
   });
   
   
});