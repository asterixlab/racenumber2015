<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="rc_front_top">
<div id="rc_header_menu">
	<div id="rc_header_menu_logo">
		<a href=""><img src="/wp-content/uploads/main_logo.png"></a>
	</div>
	<div id="rc_header_menu_right">
		<div id="rc_submenu_space">
			<ul>
				<li>
					<div id="social_icons"><a href=""><img src="/wp-content/uploads/facebook_icon.png"></a></div>
				</li>
				<li>
					<div id="social_icons"><a href="https://twitter.com/race_number"><img src="/wp-content/uploads/twitter_icon.png"></a></div>
				</li>
			</ul>
		</div>
		<div id="rc_menu_space">
			<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
		</div>
	</div>
</div><!--rc_header_menu-->

</div><!--rc_front_top-->

<div id="page" class="hfeed site">
	

	<div id="main" class="wrapper">