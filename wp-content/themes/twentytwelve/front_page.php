<?php /* Template Name: front_page_only


*/ ?>

<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>

</head>
<style>
body {
min-width:1080px;
}

.site {
		margin: 0 auto;
		width: 1080px;
		min-height:200px;
		/*max-width: 68.571428571rem;*/
		overflow: hidden;
	}
/*#rc_front_top {
width:100%;
min-height:265px;
background:url(/wp-content/uploads/newheaderbg.jpg) no-repeat center;*/
}
.entry-header {
	margin-top:0px;
	margin-bottom: 24px;
	margin-bottom: 1.714285714rem;
}
</style>
<body <?php body_class(); ?>>

<div id="rc_front_top">

<div id="rc_header_menu">
	<div id="rc_header_menu_logo">
		<a href=""><img src="/wp-content/uploads/main_logo.png"></a>
	</div>
	<div id="rc_header_menu_right">
		<div id="rc_submenu_space">
			<ul>
				<li>
					<div id="social_icons"><a href=""><img src="/wp-content/uploads/facebook_icon.png"></a></div>
				</li>
				<li>
					<div id="social_icons"><a href="https://twitter.com/race_number"><img src="/wp-content/uploads/twitter_icon.png"></a></div>
				</li>
			</ul>
		</div>
		<div id="rc_menu_space">
			<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
		</div>
	</div>
</div><!--rc_header_menu-->


</div><!--rc_front_top-->

<div id="page" class="hfeed site">
	
	<div id="main" class="wrapper">

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>



				<?php get_template_part( 'content', 'page' ); ?>
				
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php
/**
 * The sidebar containing the main widget area
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<div id="secondary" class="widget-area" role="complementary">
			<div id="sidebarsearchlink">
				<a href="index.php/events-2/">Race Search</a>
			</div>
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div><!-- #secondary -->
	<?php endif; ?>




</div><!-- #main .wrapper -->
</div><!-- #page -->

<div id="rn_footer">
	<div id="rn_footer_menu">
		<div id="rn_footer_menu_copyright">
		© 2014 Copyright <strong>RaceNumber.co.uk</strong>
		</div>
		<div id="rn_footer_menu_quick">
		<?php wp_nav_menu( array('menu' => 'footer_menu1' )); ?>
		</div>
	</div>
</div><!--rn_footer-->


<?php wp_footer(); ?>
</body>
</html>