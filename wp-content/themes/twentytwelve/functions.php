<?php

/**

 * Twenty Twelve functions and definitions

 *

 * Sets up the theme and provides some helper functions, which are used

 * in the theme as custom template tags. Others are attached to action and

 * filter hooks in WordPress to change core functionality.

 *

 * When using a child theme (see http://codex.wordpress.org/Theme_Development and

 * http://codex.wordpress.org/Child_Themes), you can override certain functions

 * (those wrapped in a function_exists() call) by defining them first in your child theme's

 * functions.php file. The child theme's functions.php file is included before the parent

 * theme's file, so the child theme functions would be used.

 *

 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached

 * to a filter or action hook.

 *

 * For more information on hooks, actions, and filters, @link http://codex.wordpress.org/Plugin_API

 *

 * @package WordPress

 * @subpackage Twenty_Twelve

 * @since Twenty Twelve 1.0

 */



// Set up the content width value based on the theme's design and stylesheet.

if ( ! isset( $content_width ) )

	$content_width = 757;



/**

 * Twenty Twelve setup.

 *

 * Sets up theme defaults and registers the various WordPress features that

 * Twenty Twelve supports.

 *

 * @uses load_theme_textdomain() For translation/localization support.

 * @uses add_editor_style() To add a Visual Editor stylesheet.

 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,

 * 	custom background, and post formats.

 * @uses register_nav_menu() To add support for navigation menus.

 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.

 *

 * @since Twenty Twelve 1.0

 */

function twentytwelve_setup() {

	/*

	 * Makes Twenty Twelve available for translation.

	 *

	 * Translations can be added to the /languages/ directory.

	 * If you're building a theme based on Twenty Twelve, use a find and replace

	 * to change 'twentytwelve' to the name of your theme in all the template files.

	 */

	load_theme_textdomain( 'twentytwelve', get_template_directory() . '/languages' );



	// This theme styles the visual editor with editor-style.css to match the theme style.

	add_editor_style();



	// Adds RSS feed links to <head> for posts and comments.

	add_theme_support( 'automatic-feed-links' );



	// This theme supports a variety of post formats.

	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );



	// This theme uses wp_nav_menu() in one location.

	register_nav_menu( 'primary', __( 'Primary Menu', 'twentytwelve' ) );



	/*

	 * This theme supports custom background color and image,

	 * and here we also set up the default background color.

	 */

	add_theme_support( 'custom-background', array(

		'default-color' => 'e6e6e6',

	) );



	// This theme uses a custom image size for featured images, displayed on "standard" posts.

	add_theme_support( 'post-thumbnails' );

	set_post_thumbnail_size( 750, 9999 ); // Unlimited height, soft crop

}

add_action( 'after_setup_theme', 'twentytwelve_setup' );



/**

 * Add support for a custom header image.

 */

require( get_template_directory() . '/inc/custom-header.php' );



/**

 * Return the Google font stylesheet URL if available.

 *

 * The use of Open Sans by default is localized. For languages that use

 * characters not supported by the font, the font can be disabled.

 *

 * @since Twenty Twelve 1.2

 *

 * @return string Font stylesheet or empty string if disabled.

 */

function twentytwelve_get_font_url() {

	$font_url = '';



	/* translators: If there are characters in your language that are not supported

	 * by Open Sans, translate this to 'off'. Do not translate into your own language.

	 */

	if ( 'off' !== _x( 'on', 'Open Sans font: on or off', 'twentytwelve' ) ) {

		$subsets = 'latin,latin-ext';



		/* translators: To add an additional Open Sans character subset specific to your language,

		 * translate this to 'greek', 'cyrillic' or 'vietnamese'. Do not translate into your own language.

		 */

		$subset = _x( 'no-subset', 'Open Sans font: add new subset (greek, cyrillic, vietnamese)', 'twentytwelve' );



		if ( 'cyrillic' == $subset )

			$subsets .= ',cyrillic,cyrillic-ext';

		elseif ( 'greek' == $subset )

			$subsets .= ',greek,greek-ext';

		elseif ( 'vietnamese' == $subset )

			$subsets .= ',vietnamese';



		$protocol = is_ssl() ? 'https' : 'http';

		$query_args = array(

			'family' => 'Open+Sans:400italic,700italic,400,700',

			'subset' => $subsets,

		);

		$font_url = add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" );

	}



	return $font_url;

}



/**

 * Enqueue scripts and styles for front-end.

 *

 * @since Twenty Twelve 1.0

 *

 * @return void

 */

function twentytwelve_scripts_styles() {

	global $wp_styles;



	/*

	 * Adds JavaScript to pages with the comment form to support

	 * sites with threaded comments (when in use).

	 */

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )

		wp_enqueue_script( 'comment-reply' );



	// Adds JavaScript for handling the navigation menu hide-and-show behavior.

	wp_enqueue_script( 'twentytwelve-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '1.0', true );



	$font_url = twentytwelve_get_font_url();

	if ( ! empty( $font_url ) )

		wp_enqueue_style( 'twentytwelve-fonts', esc_url_raw( $font_url ), array(), null );



	// Loads our main stylesheet.

	wp_enqueue_style( 'twentytwelve-style', get_stylesheet_uri() );



	// Loads the Internet Explorer specific stylesheet.

	wp_enqueue_style( 'twentytwelve-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentytwelve-style' ), '20121010' );

	$wp_styles->add_data( 'twentytwelve-ie', 'conditional', 'lt IE 9' );

}

add_action( 'wp_enqueue_scripts', 'twentytwelve_scripts_styles' );



/**

 * Filter TinyMCE CSS path to include Google Fonts.

 *

 * Adds additional stylesheets to the TinyMCE editor if needed.

 *

 * @uses twentytwelve_get_font_url() To get the Google Font stylesheet URL.

 *

 * @since Twenty Twelve 1.2

 *

 * @param string $mce_css CSS path to load in TinyMCE.

 * @return string Filtered CSS path.

 */

function twentytwelve_mce_css( $mce_css ) {

	$font_url = twentytwelve_get_font_url();



	if ( empty( $font_url ) )

		return $mce_css;



	if ( ! empty( $mce_css ) )

		$mce_css .= ',';



	$mce_css .= esc_url_raw( str_replace( ',', '%2C', $font_url ) );



	return $mce_css;

}

add_filter( 'mce_css', 'twentytwelve_mce_css' );



/**

 * Filter the page title.

 *

 * Creates a nicely formatted and more specific title element text

 * for output in head of document, based on current view.

 *

 * @since Twenty Twelve 1.0

 *

 * @param string $title Default title text for current view.

 * @param string $sep Optional separator.

 * @return string Filtered title.

 */

function twentytwelve_wp_title( $title, $sep ) {

	global $paged, $page;



	if ( is_feed() )

		return $title;



	// Add the site name.

	$title .= get_bloginfo( 'name' );



	// Add the site description for the home/front page.

	$site_description = get_bloginfo( 'description', 'display' );

	if ( $site_description && ( is_home() || is_front_page() ) )

		$title = "$title $sep $site_description";



	// Add a page number if necessary.

	if ( $paged >= 2 || $page >= 2 )

		$title = "$title $sep " . sprintf( __( 'Page %s', 'twentytwelve' ), max( $paged, $page ) );



	return $title;

}

add_filter( 'wp_title', 'twentytwelve_wp_title', 10, 2 );



/**

 * Filter the page menu arguments.

 *

 * Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link.

 *

 * @since Twenty Twelve 1.0

 */

function twentytwelve_page_menu_args( $args ) {

	if ( ! isset( $args['show_home'] ) )

		$args['show_home'] = true;

	return $args;

}

add_filter( 'wp_page_menu_args', 'twentytwelve_page_menu_args' );



/**

 * Register sidebars.

 *

 * Registers our main widget area and the front page widget areas.

 *

 * @since Twenty Twelve 1.0

 */

function twentytwelve_widgets_init() {

	register_sidebar( array(

		'name' => __( 'Main Sidebar', 'twentytwelve' ),

		'id' => 'sidebar-1',

		'description' => __( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', 'twentytwelve' ),

		'before_widget' => '<aside id="%1$s" class="widget %2$s">',

		'after_widget' => '</aside>',

		'before_title' => '<h3 class="widget-title">',

		'after_title' => '</h3>',

	) );



	register_sidebar( array(

		'name' => __( 'First Front Page Widget Area', 'twentytwelve' ),

		'id' => 'sidebar-2',

		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'twentytwelve' ),

		'before_widget' => '<aside id="%1$s" class="widget %2$s">',

		'after_widget' => '</aside>',

		'before_title' => '<h3 class="widget-title">',

		'after_title' => '</h3>',

	) );



	register_sidebar( array(

		'name' => __( 'Second Front Page Widget Area', 'twentytwelve' ),

		'id' => 'sidebar-3',

		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'twentytwelve' ),

		'before_widget' => '<aside id="%1$s" class="widget %2$s">',

		'after_widget' => '</aside>',

		'before_title' => '<h3 class="widget-title">',

		'after_title' => '</h3>',

	) );

}

add_action( 'widgets_init', 'twentytwelve_widgets_init' );



if ( ! function_exists( 'twentytwelve_content_nav' ) ) :

/**

 * Displays navigation to next/previous pages when applicable.

 *

 * @since Twenty Twelve 1.0

 */

function twentytwelve_content_nav( $html_id ) {

	global $wp_query;



	$html_id = esc_attr( $html_id );



	if ( $wp_query->max_num_pages > 1 ) : ?>

		<nav id="<?php echo $html_id; ?>" class="navigation" role="navigation">

			<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentytwelve' ); ?></h3>

			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentytwelve' ) ); ?></div>

			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentytwelve' ) ); ?></div>

		</nav><!-- #<?php echo $html_id; ?> .navigation -->

	<?php endif;

}

endif;



if ( ! function_exists( 'twentytwelve_comment' ) ) :

/**

 * Template for comments and pingbacks.

 *

 * To override this walker in a child theme without modifying the comments template

 * simply create your own twentytwelve_comment(), and that function will be used instead.

 *

 * Used as a callback by wp_list_comments() for displaying the comments.

 *

 * @since Twenty Twelve 1.0

 *

 * @return void

 */

function twentytwelve_comment( $comment, $args, $depth ) {

	$GLOBALS['comment'] = $comment;

	switch ( $comment->comment_type ) :

		case 'pingback' :

		case 'trackback' :

		// Display trackbacks differently than normal comments.

	?>

	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">

		<p><?php _e( 'Pingback:', 'twentytwelve' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?></p>

	<?php

			break;

		default :

		// Proceed with normal comments.

		global $post;

	?>

	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">

		<article id="comment-<?php comment_ID(); ?>" class="comment">

			<header class="comment-meta comment-author vcard">

				<?php

					echo get_avatar( $comment, 44 );

					printf( '<cite><b class="fn">%1$s</b> %2$s</cite>',

						get_comment_author_link(),

						// If current post author is also comment author, make it known visually.

						( $comment->user_id === $post->post_author ) ? '<span>' . __( 'Post author', 'twentytwelve' ) . '</span>' : ''

					);

					printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',

						esc_url( get_comment_link( $comment->comment_ID ) ),

						get_comment_time( 'c' ),

						/* translators: 1: date, 2: time */

						sprintf( __( '%1$s at %2$s', 'twentytwelve' ), get_comment_date(), get_comment_time() )

					);

				?>

			</header><!-- .comment-meta -->



			<?php if ( '0' == $comment->comment_approved ) : ?>

				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'twentytwelve' ); ?></p>

			<?php endif; ?>



			<section class="comment-content comment">

				<?php comment_text(); ?>

				<?php edit_comment_link( __( 'Edit', 'twentytwelve' ), '<p class="edit-link">', '</p>' ); ?>

			</section><!-- .comment-content -->



			<div class="reply">

				<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'twentytwelve' ), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>

			</div><!-- .reply -->

		</article><!-- #comment-## -->

	<?php

		break;

	endswitch; // end comment_type check

}

endif;



if ( ! function_exists( 'twentytwelve_entry_meta' ) ) :

/**

 * Set up post entry meta.

 *

 * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.

 *

 * Create your own twentytwelve_entry_meta() to override in a child theme.

 *

 * @since Twenty Twelve 1.0

 *

 * @return void

 */

function twentytwelve_entry_meta() {

	// Translators: used between list items, there is a space after the comma.

	$categories_list = get_the_category_list( __( ', ', 'twentytwelve' ) );



	// Translators: used between list items, there is a space after the comma.

	$tag_list = get_the_tag_list( '', __( ', ', 'twentytwelve' ) );



	$date = sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>',

		esc_url( get_permalink() ),

		esc_attr( get_the_time() ),

		esc_attr( get_the_date( 'c' ) ),

		esc_html( get_the_date() )

	);



	$author = sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',

		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),

		esc_attr( sprintf( __( 'View all posts by %s', 'twentytwelve' ), get_the_author() ) ),

		get_the_author()

	);



	// Translators: 1 is category, 2 is tag, 3 is the date and 4 is the author's name.

	if ( $tag_list ) {

		$utility_text = __( 'This entry was posted in %1$s and tagged %2$s on %3$s<span class="by-author"> by %4$s</span>.', 'twentytwelve' );

	} elseif ( $categories_list ) {

		$utility_text = __( 'This entry was posted in %1$s on %3$s<span class="by-author"> by %4$s</span>.', 'twentytwelve' );

	} else {

		$utility_text = __( 'This entry was posted on %3$s<span class="by-author"> by %4$s</span>.', 'twentytwelve' );

	}



	printf(

		$utility_text,

		$categories_list,

		$tag_list,

		$date,

		$author

	);

}

endif;



/**

 * Extend the default WordPress body classes.

 *

 * Extends the default WordPress body class to denote:

 * 1. Using a full-width layout, when no active widgets in the sidebar

 *    or full-width template.

 * 2. Front Page template: thumbnail in use and number of sidebars for

 *    widget areas.

 * 3. White or empty background color to change the layout and spacing.

 * 4. Custom fonts enabled.

 * 5. Single or multiple authors.

 *

 * @since Twenty Twelve 1.0

 *

 * @param array $classes Existing class values.

 * @return array Filtered class values.

 */

function twentytwelve_body_class( $classes ) {

	$background_color = get_background_color();

	$background_image = get_background_image();



	if ( ! is_active_sidebar( 'sidebar-1' ) || is_page_template( 'page-templates/full-width.php' ) )

		$classes[] = 'full-width';



	if ( is_page_template( 'page-templates/front-page.php' ) ) {

		$classes[] = 'template-front-page';

		if ( has_post_thumbnail() )

			$classes[] = 'has-post-thumbnail';

		if ( is_active_sidebar( 'sidebar-2' ) && is_active_sidebar( 'sidebar-3' ) )

			$classes[] = 'two-sidebars';

	}



	if ( empty( $background_image ) ) {

		if ( empty( $background_color ) )

			$classes[] = 'custom-background-empty';

		elseif ( in_array( $background_color, array( 'fff', 'ffffff' ) ) )

			$classes[] = 'custom-background-white';

	}



	// Enable custom font class only if the font CSS is queued to load.

	if ( wp_style_is( 'twentytwelve-fonts', 'queue' ) )

		$classes[] = 'custom-font-enabled';



	if ( ! is_multi_author() )

		$classes[] = 'single-author';



	return $classes;

}

add_filter( 'body_class', 'twentytwelve_body_class' );



/**

 * Adjust content width in certain contexts.

 *

 * Adjusts content_width value for full-width and single image attachment

 * templates, and when there are no active widgets in the sidebar.

 *

 * @since Twenty Twelve 1.0

 *

 * @return void

 */

function twentytwelve_content_width() {

	if ( is_page_template( 'page-templates/full-width.php' ) || is_attachment() || ! is_active_sidebar( 'sidebar-1' ) ) {

		global $content_width;

		$content_width = 960;

	}

}

add_action( 'template_redirect', 'twentytwelve_content_width' );



/**

 * Register postMessage support.

 *

 * Add postMessage support for site title and description for the Customizer.

 *

 * @since Twenty Twelve 1.0

 *

 * @param WP_Customize_Manager $wp_customize Customizer object.

 * @return void

 */

function twentytwelve_customize_register( $wp_customize ) {

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';

	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';

	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

}

add_action( 'customize_register', 'twentytwelve_customize_register' );



/**

 * Enqueue Javascript postMessage handlers for the Customizer.

 *

 * Binds JS handlers to make the Customizer preview reload changes asynchronously.

 *

 * @since Twenty Twelve 1.0

 *

 * @return void

 */

function twentytwelve_customize_preview_js() {

	wp_enqueue_script( 'twentytwelve-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20130301', true );

}

add_action( 'customize_preview_init', 'twentytwelve_customize_preview_js' );









/**

* add some conditional output conditions for Events Manager

* @param string $replacement

* @param string $condition

* @param string $match

* @param object $EM_Event

* @return string

*/

function filterEventOutputCondition($replacement, $condition, $match, $EM_Event){

    if (is_object($EM_Event)) {

 

        switch ($condition) {

 

            // replace LF with HTML line breaks

            case 'nl2br':

                // remove conditional

                $replacement = preg_replace('/\{\/?nl2br\}/', '', $match);

                // process any placeholders and replace LF

                $replacement = nl2br($EM_Event->output($replacement));

                break;

 

            // #_ATT{Website}

            case 'has_att_event_website':

                if (is_array($EM_Event->event_attributes) && !empty($EM_Event->event_attributes['event_website']) && $EM_Event->event_attributes['event_website'] != null)

                    $replacement = preg_replace('/\{\/?has_att_event_website\}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_traffic_free':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_traffic_free'] != "Not Applicable" && $EM_Event->event_attributes['e_traffic_free'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_traffic_free}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_water_station':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_water_station'] != "Not Applicable" && $EM_Event->event_attributes['e_water_station'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_water_station}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_Toilets':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_Toilets'] != "Not Applicable" && $EM_Event->event_attributes['e_Toilets'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_Toilets}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_changing_facilities':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_changing_facilities'] != "Not Applicable" && $EM_Event->event_attributes['e_changing_facilities'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_changing_facilities}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_onsite_parking':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_onsite_parking'] != "Not Applicable" && $EM_Event->event_attributes['e_onsite_parking'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_onsite_parking}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_refreshments':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_refreshments'] != "Not Applicable" && $EM_Event->event_attributes['e_refreshments'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_refreshments}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_supervised_bag_storage':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_supervised_bag_storage'] != "Not Applicable" && $EM_Event->event_attributes['e_supervised_bag_storage'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_supervised_bag_storage}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_free_drinks_at_the_finish':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_free_drinks_at_the_finish'] != "Not Applicable" && $EM_Event->event_attributes['e_free_drinks_at_the_finish'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_free_drinks_at_the_finish}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_first_aid':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_first_aid'] != "Not Applicable" && $EM_Event->event_attributes['e_first_aid'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_first_aid}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_course_option':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_course_option'] != "Not Applicable" && $EM_Event->event_attributes['e_course_option'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_course_option}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_chip_timing':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_chip_timing'] != "Not Applicable" && $EM_Event->event_attributes['e_chip_timing'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_chip_timing}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_Prizes':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['Prizes'] != "" && $EM_Event->event_attributes['Prizes'] != null)

                    $replacement = preg_replace('/\{\/?has_att_Prizes}/', '', $match);

                else

                    $replacement = '';

                break;

 

        }

 

    }

 

    return $replacement;

}

 

add_filter('em_event_output_condition', 'filterEventOutputCondition', 10, 4);







add_filter('em_events_get','my_em_styles_events_get',1,2);

function my_em_styles_events_get($events, $args){

	if( !empty($args['style']) && is_numeric($args['style']) ){

		foreach($events as $event_key => $EM_Event){

			if( !in_array($args['style'],$EM_Event->styles) ){

				unset($events[$event_key]);

			}

		}

	}

	return $events;

}



/*CuongBui*/



if( ! function_exists( 'add_loginout_to_menu' ) ) {

    function add_loginout_to_menu( $items, $args ){

        //Nav location in your theme. In this case, primary nav. Adjust accordingly.

        if( is_admin() ||  $args->theme_location != 'primary' )

            return $items; 

        if( is_user_logged_in( ) ) {

            $link = '<a href="' . wp_logout_url('/index.php') . '" title="' .  __( 'Logout' ) .'">' . __( 'Logout' ) . '</a>';

            return $items.= '<li id="loginout-link" class="menu-item menu-type-link">'. $link . '</li>';

        }

        else {

            $link = '<a id="loginlink" class="simplemodal-login" href="' . wp_login_url('/index.php') . '" title="Login">' . __( 'Login' ) . '</a>';

            return $items.= '<li id="loginout-link" class="menu-item menu-type-link">'. $link . '</li>';

        }

    }

}

add_filter( 'wp_nav_menu_items', 'add_loginout_to_menu', 10, 2 );





function cb_style() {

	echo '<link href="'.  get_template_directory_uri() .'/cb/css/style.css" rel="stylesheet" type="text/css" />';

        

        echo '<link href="http://www.racenumber.co.uk/wp-content/plugins/events-manager/includes/css/ui-lightness.css" rel="stylesheet" type="text/css" />';

}

add_filter('wp_head', 'cb_style');



function add_this_script_footer(){

    wp_enqueue_script('cbscript', get_template_directory_uri() . '/cb/js/script.js', array('jquery'));

}

add_action('wp_footer', 'add_this_script_footer');



add_filter("ws_plugin__s2member_login_redirect", "my_custom_login_redirect", 10, 2);

function my_custom_login_redirect($redirect, $vars = array())

{

        // If you want s2Member to perform the redirect, return true.

        // return true;



        // Or, if you do NOT want s2Member to perform the redirect, return false.

         return false;



        // Or, if you want s2Member to redirect, but to a custom URL, return that URL.

        // return 'http://www.example.com/reset-password-please/';



        // Or, just return what s2Member already says about the matter.

//        return $redirect;

}



add_filter('em_event_output_placeholder','my_em_styles_placeholders',1,3);

function my_em_styles_placeholders($replace, $EM_Event, $result){

	switch( $result ){

		case '#_APPLYONLINE':

                        $replace = 'none';



			$event_link = esc_url($EM_Event->get_permalink());

                        $replace = '<a href="'.$event_link.'" title="'.esc_attr($EM_Event->event_name).'">More Info</a>';

			break;

	}

	return $replace;

}



add_action( 'wp_ajax_send_emails', 'send_emails_callback' );



function send_emails_callback() {

    $eventid = $_POST['eventid'];

    $event = em_get_event($eventid);

    $currentuserid = get_current_user_id();

    

    if( is_user_logged_in() && current_user_can('edit_events')  && $event->event_owner == $currentuserid){

        $subject = $_POST['subject'];

        $content = $_POST['content'];

        $bookings = $event->get_bookings();

        $checks = array();

        foreach ($bookings as $booking) {

            $useremail = $booking->get_person()->user_email;

            $check = $event->email_send( $subject, $content, $useremail);

            array_push($checks, $check);

        }

        $message = "Emails were sent to all member successfully!";

        $success = 0;

        $fail = 0;

        foreach ($checks as $check) {

            if(!$check)

                $fail++;

            else $success++;

        }

        

        if($fail == count($checks))

            $message = "Emails were failed to send to all member!";

        else if($fail > 0 && $fail < count($checks))

            $message = "Emails were sent to some of the member only!";

        

        echo $message;

    }   

}

function getPostFileUrl_func( $atts ) {
     return wp_get_attachment_url($atts['id']);
}
add_shortcode('getPostFileUrl', 'getPostFileUrl_func');



add_action('wp_head','pluginname_ajaxurl');

function pluginname_ajaxurl() {

?>

<script type="text/javascript">

var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

</script>

<?php

}



