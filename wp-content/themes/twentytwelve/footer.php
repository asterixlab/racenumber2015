<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	</div><!-- #main .wrapper -->
	

	
</div><!-- #page -->

<div id="rn_footer">
	<div id="rn_footer_menu">
		<div id="rn_footer_menu_copyright">
		© 2014 Copyright <strong>RaceNumber.co.uk</strong>
		</div>
		<div id="rn_footer_menu_quick">
		<?php wp_nav_menu( array('menu' => 'footer_menu1' )); ?>
		</div>
	</div>
</div>

<?php wp_footer(); ?>
</body>
</html>