<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<!--<header class="entry-header">
			<div class="eventheader"><?php the_title(); ?></div>-->
			<?php if ( ! is_page_template( 'page-templates/front-page.php' ) ) : ?>
			<!--<?php the_post_thumbnail(); ?>-->
			<?php endif; ?>
			
		</header>

		<div class="entry-content">
                    <!-- CuongBui -->
			<?php 
				if(is_page(470))
				{
					echo "<h1>Add a New Race</h1>";
				}
				else if(is_page(256))
				{
					echo "<h1>My profile</h1>";
				}
				else if(is_page(241))
				{
					echo "<h1>Event Bookings</h1>";
				}
				else if(is_page(115))
				{
					//CuongBui
					echo "<h1>Current Entries</h1>";
				}
				
			?>
			
			<?php the_content(); ?>

			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'twentytwelve' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
		<footer class="entry-meta">
			<?php edit_post_link( __( 'Edit', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?>
		</footer><!-- .entry-meta -->
	</article><!-- #post -->