<?php

/*

 * This file is called by templates/forms/event-editor.php to display attribute fields on your event form on your website.

* You can override this file by copying it to /wp-content/themes/yourtheme/plugins/events-manager/forms/event/ and editing it there.

*/

global $EM_Event;

/* @var $EM_Event EM_Event */

$attributes = em_get_attributes();

//var_dump($attributes);exit;

$has_depreciated = false;

//CuongBui

?>

<?php if( count( $attributes['names'] ) > 0 ) : ?>

	<?php foreach( $attributes['names'] as $name) : ?>

        <?php 

            if($name != "e_description" && $name != "prize_detail" && $name != "e_postal_entry" && $name != "download_entry_form"){ 

                $type = "text";

                $label = $name;

                $placeholder = "";

                if($name == "event_website")

                    $label = "Event website";



				 else if($name == "e_organiser"){

                    $label = "Event Organiser (For example: Gateshead Harriers, Race Events Limited)";


                }

                else if($name == "e_terrain"){

                    $label = "Terrain";

                    $type = "dropdown";

                }

                else if($name == "Profile"){

                    $label = "Profile";

                    $type = "dropdown";

                }

                else if($name == "course_records_mens"){

                    $label = "Course record (mens)";

                    $placeholder = "Unknown";

                }

                else if($name == "course_records_womens"){

                    $label = "Course record (womens)";

                    $placeholder = "Unknown";

                }

                else if($name == "last_years_winning_time_mens"){

                    $label = "Last years winning time (mens)";

                    $placeholder = "Unknown";

                }

                else if($name == "last_years_winning_time_womens"){

                    $label = "Last years winning time (womens)";

                    $placeholder = "Unknown";

                }

                else if($name == "Prizes"){

                    $label = "Prizes";

                    $type = "textarea";

                }

                else if($name == "download_entry_form")

                    $label = "Additional info";

                else if($name == "e_entry_accepted"){

                    $label = "On the day entry accepted";

                    $type = "dropdown";

                }

                else if($name == "e_additional_cost"){

                    $label = "Additional Cost";

                    $type = "text";

                }

                else if($name == "e_course_option"){

                    $label = "Course option";

                    $type = "dropdown";

                }

                else if($name == "e_traffic_free"){

                    $label = "Trafffic free";

                    $type = "dropdown";

                }

                else if($name == "e_water_station"){

                    $label = "Water On The Course";

                    $type = "dropdown";

                }

                else if($name == "e_Toilets"){

                    $label = "Toilets";

                    $type = "dropdown";

                }

                else if($name == "e_changing_facilities"){

                    $label = "Changing facilities";

                    $type = "dropdown";

                }

                else if($name == "e_onsite_parking"){

                    $label = "Onsite parking";

                    $type = "dropdown";

                }

                else if($name == "e_refreshments"){

                    $label = "Refreshments";

                    $type = "dropdown";

                }

                else if($name == "e_supervised_bag_storage"){

                    $label = "Supervised bag storage";

                    $type = "dropdown";

                }

                else if($name == "e_free_drinks_at_the_finish"){

                    $label = "Free drinks at finish";

                    $type = "dropdown";

                }

                else if($name == "e_first_aid"){

                    $label = "First aid coverage";

                    $type = "dropdown";

                }

                else if($name == "e_chip_timing"){

                    $label = "Chip timing";

                    $type = "dropdown";

                }

                else if($name == "e_minimum_age"){

                    $label = "Minimum age";

                    $type = "text";

                } 

                else if($name == "e_entry_limit"){

                    $label = "Entry limit";

                    $type = "text";

                }
                
                else if($name == "e_organiser_contact"){

                    $label = "Organiser Contact";

                    $type = "text";

                }

                else if($name == "Category")

                    $label = "Distance";

        ?>

	<div class="event-attributes">

		<label for="em_attributes[<?php echo $name ?>]"><?php echo $label ?></label>

		<?php // if( count($attributes['values'][$name]) > 1 ): ?>

                <?php if( $type == "dropdown" ){ ?>

                <br/>

		<select style="width: 150px; height: 30px;" name="em_attributes[<?php echo $name ?>]">

			<?php foreach($attributes['values'][$name] as $attribute_val): ?>

				<?php if( is_array($EM_Event->event_attributes) && array_key_exists($name, $EM_Event->event_attributes) && $EM_Event->event_attributes[$name]==$attribute_val ): ?>

					<option selected="selected"><?php echo $attribute_val; ?></option>

				<?php else: ?>

					<option><?php echo $attribute_val; ?></option>

				<?php endif; ?>

			<?php endforeach; ?>

		</select>

                <?php } else if($type == "textarea"){ ?>

                <br/>

                <textarea cols="30" rows="5" name="em_attributes[<?php echo $name ?>]"><?php echo array_key_exists($name, $EM_Event->event_attributes) ? esc_attr($EM_Event->event_attributes[$name], ENT_QUOTES):''; ?></textarea>

		<?php } else { ?>

		<?php $default_value = (!empty($attributes['values'][$name][0])) ? $attributes['values'][$name][0]:''; ?>

                <br/>

		<input placeholder="<?php echo $placeholder; ?>" type="text" name="em_attributes[<?php echo $name ?>]" value="<?php echo array_key_exists($name, $EM_Event->event_attributes) ? esc_attr($EM_Event->event_attributes[$name], ENT_QUOTES):''; ?>" value="<?php echo $default_value; ?>" />

		<?php } ?>

	</div>

        <?php } ?>

	<?php endforeach; ?>

        <br/>

<?php endif; ?>