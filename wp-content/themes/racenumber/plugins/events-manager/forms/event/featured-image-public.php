<?php
/*
* This file is called by templates/forms/location-editor.php to display fields for uploading images on your event form on your website. This does not affect the admin featured image section.
* You can override this file by copying it to /wp-content/themes/yourtheme/plugins/events-manager/forms/event/ and editing it there.
*/
global $EM_Event;
/* @var $EM_Event EM_Event */
?>
<p id="event-image-img">
<?php if ($EM_Event->get_image_url() != '') : ?>
	<img src='<?php echo $EM_Event->get_image_url('medium'); ?>' alt='<?php echo $EM_Event->event_name ?>'/>
<?php else : ?> 
	<?php _e('No image uploaded for this event yet', 'dbem') ?>
<?php endif; ?>
</p>
<label for='event_image' id="lblimage"><?php _e('Upload/change picture', 'dbem') ?></label> <input id='event-image' name='event_image' id='event_image' type='file' size='40' />
<script type="text/javascript">
    jQuery(function($){
        $(document).on('change', '#event-image', function() {
            if(this.files[0].size > 2097152){
                this.remove();
                $("#lblimage").after('<input class="inputimage" type="file" size="40" name="event_image" id="event-image">');
                alert("Sorry your image is to high in resolution for our site. We recommend using this website (http://www.picresize.com) to resize your image down to 500px in width: Once you've done that, log back in and edit your event to add your image. Sorry for the trouble J");
            }
          });
    });
    
</script>
<br />
<?php if ($EM_Event->get_image_url() != '') : ?>
<label for='event_image_delete'><?php _e('Delete Image?', 'dbem') ?></label> <input id='event-image-delete' name='event_image_delete' id='event_image_delete' type='checkbox' value='1' />
<?php endif; ?>