	<?php global $theme_option; ?>
	<div class="clear" ></div>
	</div><!-- content wrapper -->

	<?php 
		// page style
		global $gdlr_post_option;
		if( empty($gdlr_post_option) || empty($gdlr_post_option['page-style']) ||
			  $gdlr_post_option['page-style'] == 'normal' || 
			  $gdlr_post_option['page-style'] == 'no-header'){ 
	?>	
	<footer class="footer-wrapper" >
		<?php if( $theme_option['show-footer'] != 'disable' ){ ?>
		<div class="footer-container container">
			<?php 	
				$i = 1;
				$theme_option['footer-layout'] = empty($theme_option['footer-layout'])? '1': $theme_option['footer-layout'];
				$gdlr_footer_layout = array(
					'1'=>array('twelve columns'),
					'2'=>array('three columns', 'three columns', 'three columns', 'three columns'),
					'3'=>array('three columns', 'three columns', 'six columns',),
					'4'=>array('four columns', 'four columns', 'four columns'),
					'5'=>array('four columns', 'four columns', 'eight columns'),
					'6'=>array('eight columns', 'four columns', 'four columns'),
				);
			?>
			<?php foreach( $gdlr_footer_layout[$theme_option['footer-layout']] as $footer_class ){ ?>
				<div class="footer-column <?php echo $footer_class; ?>" id="footer-widget-<?php echo $i; ?>" >
					<?php dynamic_sidebar('Footer ' . $i); ?>
				</div>
			<?php $i++; ?>
			<?php } ?>
			<div class="clear"></div>
		</div>
		<?php } ?>
		
		<?php if( $theme_option['show-copyright'] != 'disable' ){ ?>
		<div class="copyright-wrapper">
			<div class="copyright-container container">
				<div class="copyright-left">
					<?php if( !empty($theme_option['copyright-left-text']) ) echo $theme_option['copyright-left-text']; ?>
<div><strong>&#169; Copyright Race Number 2014</strong> | <a href="http://www.racenumber.co.uk/terms-and-conditions/"><font color="#fff">Terms & Conditions</font></a> | <a href="http://www.racenumber.co.uk/privacy-policy/"><font color="#fff">Privacy Policy</font></a> |<a href="http://www.racenumber.co.uk/refunds/"><font color="#fff">Refunds</font></a> | <a href="http://www.racenumber.co.uk/contact/"><font color="#fff">Contact</font></a></div>
				</div>
				<div class="copyright-right">
					<?php if( !empty($theme_option['copyright-right-text']) ) echo $theme_option['copyright-right-text']; ?>
<i class="gdlr-icon-lock icon-lock" style="color: #fff; font-size: 14px; "></i><a href="/wp-login.php?action=register" class="simplemodal-register"><font color="#fff">Sign Up</font></a> | <?php if (is_user_logged_in()) : ?>
    <a href="<?php echo wp_logout_url(get_permalink()); ?>"><font color="#fff">Sign Out</font></a>
<?php else : ?>
    <a href="<?php echo wp_login_url(get_permalink()); ?>"><font color="#fff">Sign In</font></a>
<?php endif;?>
					<?php gdlr_print_header_social(); ?>
					</div>
					<?php 
						if( function_exists('gdlr_lms_header_signin') ){
							gdlr_lms_header_signin();
						}
					?>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<?php } ?>
	</footer>
	<?php } // page style ?>
</div> <!-- body-wrapper -->
<?php wp_footer(); ?>
</body>
</html>