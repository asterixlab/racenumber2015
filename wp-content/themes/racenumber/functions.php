<?php
	/*	
	*	Goodlayers Function File
	*	---------------------------------------------------------------------
	*	This file include all of important function and features of the theme
	*	---------------------------------------------------------------------
	*/
	
	////// DO NOT REMOVE OR MODIFY THIS /////
	define('WP_THEME_KEY', 'goodlayers');  //
	/////////////////////////////////////////
	
	define('THEME_FULL_NAME', 'Clever Course');
	define('THEME_SHORT_NAME', 'clvc');
	define('THEME_SLUG', 'clevercourse');
	
	define('AJAX_URL', admin_url('admin-ajax.php'));
	define('GDLR_PATH', get_template_directory_uri());
	define('GDLR_LOCAL_PATH', get_template_directory());
	
	if ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443 ) {
		define('GDLR_HTTP', 'https://');
	}else{
		define('GDLR_HTTP', 'http://');
	}
	
	$gdlr_gallery_id = 0;
	$gdlr_lightbox_id = 0;
	$gdlr_crop_video = false;
	$gdlr_excerpt_length = 55;
	$gdlr_excerpt_read_more = true;

	$gdlr_spaces = array(
		'top-wrapper' => '70px', 
		'bottom-wrapper'=>'40px', 
		'top-full-wrapper' => '0px', 
		'bottom-item' => '20px',
		'bottom-blog-item' => '0px',
		'bottom-divider-item' => '50px'
	);
	
	$theme_option = get_option(THEME_SHORT_NAME . '_admin_option', array());
	$theme_option['content-width'] = 960;
	
	// include goodlayers framework
	include_once( 'framework/gdlr-framework.php' );
	
	//-------------------------- theme section ---------------------------//

	// create sidebar controller
	$gdlr_sidebar_controller = new gdlr_sidebar_generator();	
	
	// create font controller
	if( empty($theme_option['upload-font']) ){ $theme_option['upload-font'] = ''; }
	$gdlr_font_controller = new gdlr_font_loader( json_decode($theme_option['upload-font'], true) );	
	
	// create navigation controller
	if( empty($theme_option['enable-goodlayers-navigation']) || $theme_option['enable-goodlayers-navigation'] != 'disable'){
		include_once( 'include/gdlr-navigation-menu.php');
	}	
	if( empty($theme_option['enable-goodlayers-mobile-navigation']) || $theme_option['enable-goodlayers-mobile-navigation'] != 'disable'){
		include_once( 'include/gdlr-responsive-menu.php');
	}
	
	// utility function
	include_once( 'include/function/gdlr-media.php');
	include_once( 'include/function/gdlr-utility.php');		

	// register function / filter / action
	include_once( 'functions-size.php');	
	include_once( 'include/gdlr-include-script.php');	
	include_once( 'include/function/gdlr-function-regist.php');	
	
	// create admin option
	include_once( 'include/gdlr-admin-option.php');
	include_once( 'include/gdlr-plugin-option.php');
	include_once( 'include/gdlr-font-controls.php');
	include_once( 'include/gdlr-social-icon.php');

	// create page options
	include_once( 'include/gdlr-page-options.php');
	include_once( 'include/gdlr-demo-page.php');
	include_once( 'include/gdlr-post-options.php');
	
	// create page builder
	include_once( 'include/gdlr-page-builder-option.php');
	include_once( 'include/function/gdlr-page-builder.php');
	
	include_once( 'include/function/gdlr-page-item.php');
	include_once( 'include/function/gdlr-blog-item.php');
	
	// widget
	include_once( 'include/widget/recent-comment.php');
	include_once( 'include/widget/recent-post-widget.php');
	include_once( 'include/widget/popular-post-widget.php');
	include_once( 'include/widget/post-slider-widget.php');	
	include_once( 'include/widget/recent-port-widget.php');
	include_once( 'include/widget/recent-port-widget-2.php');
	include_once( 'include/widget/port-slider-widget.php');
	include_once( 'include/widget/twitter-widget.php');
	include_once( 'include/widget/flickr-widget.php');
	include_once( 'include/widget/video-widget.php');
	
	// plugin support
	include_once( 'plugins/wpml.php');
	include_once( 'plugins/layerslider.php' );
	include_once( 'plugins/masterslider.php' );
	include_once( 'plugins/woocommerce.php' );
	include_once( 'plugins/twitteroauth.php' );
	include_once( 'plugins/goodlayers-importer.php' );
	
	if( empty($theme_option['enable-plugin-recommendation']) || $theme_option['enable-plugin-recommendation'] == 'enable' ){
		include_once( 'include/plugin/gdlr-plugin-activation.php');
	}
        
        /**

* add some conditional output conditions for Events Manager

* @param string $replacement

* @param string $condition

* @param string $match

* @param object $EM_Event

* @return string

*/

function filterEventOutputCondition($replacement, $condition, $match, $EM_Event){

    if (is_object($EM_Event)) {

 

        switch ($condition) {

 

            // replace LF with HTML line breaks

            case 'nl2br':

                // remove conditional

                $replacement = preg_replace('/\{\/?nl2br\}/', '', $match);

                // process any placeholders and replace LF

                $replacement = nl2br($EM_Event->output($replacement));

                break;

 

            // #_ATT{Website}

            case 'has_att_event_website':

                if (is_array($EM_Event->event_attributes) && !empty($EM_Event->event_attributes['event_website']) && $EM_Event->event_attributes['event_website'] != null)

                    $replacement = preg_replace('/\{\/?has_att_event_website\}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_traffic_free':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_traffic_free'] != "Not Applicable" && $EM_Event->event_attributes['e_traffic_free'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_traffic_free}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_water_station':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_water_station'] != "Not Applicable" && $EM_Event->event_attributes['e_water_station'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_water_station}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_Toilets':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_Toilets'] != "Not Applicable" && $EM_Event->event_attributes['e_Toilets'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_Toilets}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_changing_facilities':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_changing_facilities'] != "Not Applicable" && $EM_Event->event_attributes['e_changing_facilities'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_changing_facilities}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_onsite_parking':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_onsite_parking'] != "Not Applicable" && $EM_Event->event_attributes['e_onsite_parking'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_onsite_parking}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_refreshments':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_refreshments'] != "Not Applicable" && $EM_Event->event_attributes['e_refreshments'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_refreshments}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_supervised_bag_storage':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_supervised_bag_storage'] != "Not Applicable" && $EM_Event->event_attributes['e_supervised_bag_storage'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_supervised_bag_storage}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_free_drinks_at_the_finish':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_free_drinks_at_the_finish'] != "Not Applicable" && $EM_Event->event_attributes['e_free_drinks_at_the_finish'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_free_drinks_at_the_finish}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_first_aid':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_first_aid'] != "Not Applicable" && $EM_Event->event_attributes['e_first_aid'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_first_aid}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_course_option':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_course_option'] != "Not Applicable" && $EM_Event->event_attributes['e_course_option'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_course_option}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_e_chip_timing':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_chip_timing'] != "Not Applicable" && $EM_Event->event_attributes['e_chip_timing'] != null)

                    $replacement = preg_replace('/\{\/?has_att_e_chip_timing}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_att_Prizes':

                if (is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['Prizes'] != "" && $EM_Event->event_attributes['Prizes'] != null)

                    $replacement = preg_replace('/\{\/?has_att_Prizes}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_download_entry_form':

                if (is_array($EM_Event->event_attributes) && getPostFileUrlFromId($EM_Event->event_attributes['download_entry_form']) != "")

                    $replacement = preg_replace('/\{\/?has_download_entry_form}/', '', $match);

                else

                    $replacement = '';

                break;

            case 'has_e_entry_cost':
                $show_condition = ($EM_Event->event_rsvp && get_option('dbem_rsvp_enabled'));
                if($show_condition){
                    $min = false;
                    $max = 0;
                    foreach( $EM_Event->get_tickets()->tickets as $EM_Ticket ){
                        /* @var $EM_Ticket EM_Ticket */
                        if( $EM_Ticket->is_available() || get_option('dbem_bookings_tickets_show_unavailable') || !empty($show_all_ticket_prices) ){
                            if($EM_Ticket->get_price() > $max ){
                                $max = $EM_Ticket->get_price();
                            }
                            if($EM_Ticket->get_price() < $min || $min === false){
                                $min = $EM_Ticket->get_price();
                            }                       
                        }
                    }

                    if($max == 0){
                        if(is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_entry_cost'] != "" && $EM_Event->event_attributes['e_entry_cost'] != null)
                            $replacement = '£' . $EM_Event->event_attributes['e_entry_cost'];
                        else if(is_array($EM_Event->event_attributes) && 
                            (($EM_Event->event_attributes['e_entry_cost_attached'] != "" && $EM_Event->event_attributes['e_entry_cost_attached'] != null && is_numeric($EM_Event->event_attributes['e_entry_cost_attached'])) || 
                            ($EM_Event->event_attributes['e_entry_cost_unattached'] != "" && $EM_Event->event_attributes['e_entry_cost_unattached'] != null && is_numeric($EM_Event->event_attributes['e_entry_cost_unattached'])))){
                            $attachedcost = intval($EM_Event->event_attributes['e_entry_cost_attached']);
                            $unattachedcost = intval($EM_Event->event_attributes['e_entry_cost_unattached']);
                            if($attachedcost == 0 && $unattachedcost != 0){
                                $replacement = '£' . $unattachedcost;
                            }
                            else if($unattachedcost == 0 && $attachedcost != 0){
                                $replacement = '£' . $attachedcost;
                            }
                            else if($unattachedcost != 0 && $attachedcost != 0){
                                $replacement = $attachedcost < $unattachedcost ? '£' . $attachedcost : '£' . $unattachedcost;
                            }
                            else $replacement = 'Unavailable';
                        }
                        else $replacement = 'Unavailable';
                    }
                    else{
                        if( $min === false ) $min = 0;
                        if( $min != $max ){
                            $replacement = em_get_currency_formatted($min).' - '.em_get_currency_formatted($max);
                        }else{
                            $replacement = em_get_currency_formatted($min);
                        }
                    }
                }
                else{
                    $show_condition = !$EM_Event->event_rsvp || $EM_Event->is_free( $condition == 'is_free_now' );
                    if ($show_condition){
                        if(is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_entry_cost'] != "" && $EM_Event->event_attributes['e_entry_cost'] != null)
                            $replacement = '£' . $EM_Event->event_attributes['e_entry_cost'];
                        else if(is_array($EM_Event->event_attributes) && 
                            (($EM_Event->event_attributes['e_entry_cost_attached'] != "" && $EM_Event->event_attributes['e_entry_cost_attached'] != null && is_numeric($EM_Event->event_attributes['e_entry_cost_attached'])) || 
                            ($EM_Event->event_attributes['e_entry_cost_unattached'] != "" && $EM_Event->event_attributes['e_entry_cost_unattached'] != null && is_numeric($EM_Event->event_attributes['e_entry_cost_unattached'])))){
                            $attachedcost = intval($EM_Event->event_attributes['e_entry_cost_attached']);
                            $unattachedcost = intval($EM_Event->event_attributes['e_entry_cost_unattached']);
                            if($attachedcost == 0 && $unattachedcost != 0){
                                $replacement = '£' . $unattachedcost;
                            }
                            else if($unattachedcost == 0 && $attachedcost != 0){
                                $replacement = '£' . $attachedcost;
                            }
                            else if($unattachedcost != 0 && $attachedcost != 0){
                                $replacement = $attachedcost < $unattachedcost ? '£' . $attachedcost : '£' . $unattachedcost;
                            }
                            else $replacement = 'Unavailable';
                        }
                        else $replacement = 'Unavailable';
                    } 
                    else $replacement = 'Unavailable';
                }

                break;

            case 'has_e_contact_email':

                if(is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_organiser_email'] != "" && $EM_Event->event_attributes['e_organiser_email'] != null)
                    $replacement = $EM_Event->event_attributes['e_organiser_email'];
                else

                    $replacement = 'Not Provided';

                break;

            case 'has_e_contact_phone':

                if(is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_organiser_phone'] != "" && $EM_Event->event_attributes['e_organiser_phone'] != null)
                    $replacement = $EM_Event->event_attributes['e_organiser_phone'];
                else

                    $replacement = 'Not Provided';

                break;

            case 'has_att_e_entries_on_the_day':
                
                if(is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_entry_accepted'] != "" && $EM_Event->event_attributes['e_entry_accepted'] != null && strtolower($EM_Event->event_attributes['e_entry_accepted']) == "yes"){
                    //$replacement = preg_replace('/\{\/?has_att_e_entries_on_the_day}/', '', $match);
                    if(is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_additional_cost'] != "" && $EM_Event->event_attributes['e_additional_cost'] != null){
                       $replacement = '( +'.$EM_Event->event_attributes['e_additional_cost'].')'; 
                    }
                    else{
                        $replacement = '';
                    }
                }
                else

                    $replacement = '';

                break;

            case 'has_e_event_date_various':
                if(is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_event_date_various'] != "" && $EM_Event->event_attributes['e_event_date_various'] != null && strtolower($EM_Event->event_attributes['e_event_date_various']) == "yes"){
                    $replacement = 'Various';
                }
                else
                    $replacement = $EM_Event->output("#_EVENTDATES");
                break;

            case 'has_not_e_event_date_various':
                if(is_array($EM_Event->event_attributes) && $EM_Event->event_attributes['e_event_date_various'] != "" && $EM_Event->event_attributes['e_event_date_various'] != null && strtolower($EM_Event->event_attributes['e_event_date_various']) == "yes"){
                    $replacement = 'Various';
                }
                else
                    $replacement = $EM_Event->output("#_{l jS F Y} at #_12HSTARTTIME");
                break;

        }

 

    }

 

    return $replacement;

}

 

add_filter('em_event_output_condition', 'filterEventOutputCondition', 10, 4);







add_filter('em_events_get','my_em_styles_events_get',1,2);

function my_em_styles_events_get($events, $args){

	if( !empty($args['style']) && is_numeric($args['style']) ){

		foreach($events as $event_key => $EM_Event){

			if( !in_array($args['style'],$EM_Event->styles) ){

				unset($events[$event_key]);

			}

		}

	}

	return $events;

}



/*CuongBui*/



if( ! function_exists( 'add_loginout_to_menu' ) ) {

    function add_loginout_to_menu( $items, $args ){

        //Nav location in your theme. In this case, primary nav. Adjust accordingly.

        if( is_admin() ||  $args->theme_location != 'primary' )

            return $items; 

        if( is_user_logged_in( ) ) {

            $link = '<a href="' . wp_logout_url('/index.php') . '" title="' .  __( 'Logout' ) .'">' . __( 'Logout' ) . '</a>';

            return $items.= '<li id="loginout-link" class="menu-item menu-type-link">'. $link . '</li>';

        }

        else {

            $link = '<a id="loginlink" class="simplemodal-login" href="' . wp_login_url('/index.php') . '" title="Login">' . __( 'Login' ) . '</a>';

            return $items.= '<li id="loginout-link" class="menu-item menu-type-link">'. $link . '</li>';

        }

    }

}

add_filter( 'wp_nav_menu_items', 'add_loginout_to_menu', 10, 2 );





function cb_style() {

	echo '<link href="'.  get_template_directory_uri() .'/cb/css/style.css" rel="stylesheet" type="text/css" />';

        

        echo '<link href="http://www.racenumber.co.uk/wp-content/plugins/events-manager/includes/css/ui-lightness.css" rel="stylesheet" type="text/css" />';

}

add_filter('wp_head', 'cb_style');



function add_this_script_footer(){

    wp_enqueue_script('cbscript', get_template_directory_uri() . '/cb/js/script.js', array('jquery'));

}

add_action('wp_footer', 'add_this_script_footer');



add_filter("ws_plugin__s2member_login_redirect", "my_custom_login_redirect", 10, 2);

function my_custom_login_redirect($redirect, $vars = array())

{

        // If you want s2Member to perform the redirect, return true.

        // return true;



        // Or, if you do NOT want s2Member to perform the redirect, return false.

         return false;



        // Or, if you want s2Member to redirect, but to a custom URL, return that URL.

        // return 'http://www.example.com/reset-password-please/';



        // Or, just return what s2Member already says about the matter.

//        return $redirect;

}



add_filter('em_event_output_placeholder','my_em_styles_placeholders',1,3);

function my_em_styles_placeholders($replace, $EM_Event, $result){

	switch( $result ){

		case '#_APPLYONLINE':

                        $replace = 'none';



			$event_link = esc_url($EM_Event->get_permalink());

                        $replace = '<a href="'.$event_link.'" title="'.esc_attr($EM_Event->event_name).'">More Info</a>';

			break;

	}

	return $replace;

}



add_action( 'wp_ajax_send_emails', 'send_emails_callback' );



function send_emails_callback() {

    $eventid = $_POST['eventid'];

    $event = em_get_event($eventid);

    $currentuserid = get_current_user_id();

    

    if( is_user_logged_in() && current_user_can('edit_events')  && $event->event_owner == $currentuserid){

        $subject = $_POST['subject'];

        $content = $_POST['content'];

        $bookings = $event->get_bookings();

        $checks = array();

        foreach ($bookings as $booking) {

            $useremail = $booking->get_person()->user_email;

            $check = $event->email_send( stripslashes($subject), stripslashes($content), $useremail);

            array_push($checks, $check);

        }

        $message = "Emails were sent to all participants successfully!";

        $success = 0;

        $fail = 0;

        foreach ($checks as $check) {

            if(!$check)

                $fail++;

            else $success++;

        }

        

        if($fail == count($checks))

            $message = "Emails were failed to send to all participants!";

        else if($fail > 0 && $fail < count($checks))

            $message = "Emails were sent to some of the participants only!";

        

        echo $message;

    }   

}

function getPostFileUrlFromId( $id ) {
     return wp_get_attachment_url($id);
}

function getPostFileUrl_func( $atts ) {
     return wp_get_attachment_url($atts['id']);
}
add_shortcode('getPostFileUrl', 'getPostFileUrl_func');

function formatTinyMCE($in)
{
    $in['remove_linebreaks']=false;
    $in['gecko_spellcheck']=false;
    $in['keep_styles']=true;
    $in['accessibility_focus']=true;
    $in['tabfocus_elements']='major-publishing-actions';
    $in['media_strict']=false;
    $in['paste_remove_styles']=false;
    $in['paste_remove_spans']=false;
    $in['paste_strip_class_attributes']='none';
    $in['paste_text_use_dialog']=true;
    $in['wpeditimage_disable_captions']=true;
    // This breaks Visual Editor in v3.9 -> $in['plugins']='inlinepopups,tabfocus,paste,media,fullscreen,wordpress,wpeditimage,wpgallery,wplink,wpdialogs,wpfullscreen';
    // $in['content_css']= "/golf/content/themes/verb_base/css/editor-style.css?v2";
    $in['wpautop']=false;
    $in['apply_source_formatting']=false;
    $in['theme_advanced_buttons1']='formatselect,forecolor,|,bold,italic,underline,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyright,|,link,unlink,|,wp_fullscreen,wp_adv';
    $in['theme_advanced_buttons2']='pastetext,pasteword,removeformat,|,charmap,|,undo,redo';
    $in['theme_advanced_buttons3']='';
    $in['theme_advanced_buttons4']='';
    return $in;
}
add_filter('tiny_mce_before_init', 'formatTinyMCE' );


add_action('wp_head','pluginname_ajaxurl');

function pluginname_ajaxurl() {

?>

<script type="text/javascript">

var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

</script>

<?php

}

	// init include script class
	if( !is_admin() ){ new gdlr_include_script(); }	
?>