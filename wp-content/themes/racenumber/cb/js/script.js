jQuery(function(){
    setTimeout(function(){
      if(jQuery('div.em-booking-form-details').length){
        var temp = jQuery('div.em-booking-form-details').find('div.em-booking-gateway-form').clone();
        jQuery('div.em-booking-form-details').find('div.em-booking-gateway-form').remove();
        jQuery('div.em-booking-form-details').prepend(temp);
      }
    }, 1000);
    

    if(jQuery("#b_donation_amount").length){
      jQuery("#b_donation_amount").before('£');
    }

    if(jQuery("#donationallowed").val() == 'Yes')
      jQuery("#donationname").css('display','block');

    jQuery(document).on("change", "#donationallowed",function(){
      if(jQuery(this).val() == 'Yes')
        jQuery("#donationname").css('display','block');
      else jQuery("#donationname").css('display','none');
    });

    if(jQuery("#ws-plugin--s2member-profile-t-shirt-size").length){
      jQuery("#ws-plugin--s2member-profile-t-shirt-size").attr("placeholder", 'XS, S, M, L, XL');
    }

    var profileSubmit = jQuery("#ws-plugin--s2member-profile-submit");
    var buttonParent = profileSubmit.parent();
    var clone = profileSubmit.clone();
    clone.attr("id", "tempprofilesubmit");
    clone.attr("type", "button");
    profileSubmit.addClass("em-hide");
    buttonParent.append(clone);

    jQuery(document).on("click", '#tempprofilesubmit', function(e){
        e.preventDefault();
        var form = jQuery(this).parents('form');
        var address1 = form.find("#ws-plugin--s2member-profile-address").val();
        // var address2 = form.find("#ws-plugin--s2member-profile-address2").val();
        var city = form.find("#ws-plugin--s2member-profile-city").val();
        var county = form.find("#ws-plugin--s2member-profile-county").val();
        var postcode = form.find("#ws-plugin--s2member-profile-postcode").val();
        // var phone = form.find("#ws-plugin--s2member-profile-phone").val();
        var dob = form.find("#ws-plugin--s2member-profile-date-of-birth").val();
        var tshirt = form.find("#ws-plugin--s2member-profile-t-shirt-size").val();
        var nationality = form.find("#ws-plugin--s2member-profile-nationality").val();
        var contactnumber = form.find("#ws-plugin--s2member-profile-emergency_contact_number").val();
        var contactname = form.find("#ws-plugin--s2member-profile-emergency_contact_name").val();
        // var clubname = form.find("#ws-plugin--s2member-profile-club").val();
        // var affiliation = form.find("#ws-plugin--s2member-profile-affiliation-number").val();
        // var club = form.find("input[name='ws_plugin__s2member_profile_affiliated']").is(":checked");
        
        if(address1 == '' || city == '' || county == '' || postcode == '' || dob == '' || nationality == '' || contactnumber == '' || contactname == ''){
          alert('Please fill in all the required fields!');
          if(address1 == '')
            form.find("#ws-plugin--s2member-profile-address").addClass('em-field-error');
          else form.find("#ws-plugin--s2member-profile-address").removeClass('em-field-error');

          if(city == '')
            form.find("#ws-plugin--s2member-profile-city").addClass('em-field-error');
          else form.find("#ws-plugin--s2member-profile-city").removeClass('em-field-error');

          if(county == '')
            form.find("#ws-plugin--s2member-profile-county").addClass('em-field-error');
          else form.find("#ws-plugin--s2member-profile-county").removeClass('em-field-error');

          if(dob == '')
            form.find("#ws-plugin--s2member-profile-date-of-birth").addClass('em-field-error');
          else form.find("#ws-plugin--s2member-profile-date-of-birth").removeClass('em-field-error');

          if(postcode == '')
            form.find("#ws-plugin--s2member-profile-postcode").addClass('em-field-error');
          else form.find("#ws-plugin--s2member-profile-postcode").removeClass('em-field-error');

          if(nationality == '')
            form.find("#ws-plugin--s2member-profile-nationality").addClass('em-field-error');
          else form.find("#ws-plugin--s2member-profile-nationality").removeClass('em-field-error');

          if(tshirt == '')
            form.find("#ws-plugin--s2member-profile-t-shirt-size").addClass('em-field-error');
          else form.find("#ws-plugin--s2member-profile-t-shirt-size").removeClass('em-field-error');

          if(contactnumber == '')
            form.find("#ws-plugin--s2member-profile-emergency_contact_number").addClass('em-field-error');
          else form.find("#ws-plugin--s2member-profile-emergency_contact_number").removeClass('em-field-error');

          if(contactname == '')
            form.find("#ws-plugin--s2member-profile-emergency_contact_name").addClass('em-field-error');
          else form.find("#ws-plugin--s2member-profile-emergency_contact_name").removeClass('em-field-error');

          return false;
        }
        else{
          profileSubmit.click();
        }
    });

    var img = jQuery(".event_note_image img");
    if(img.length){
        var width = img.width();
        var height = img.height();
        if(width > height){
            img.css("width", "350px");
        }
        else{
            // img.css("height", "450px");
        }
    }
    
    jQuery(document).on("click", ".events-table tr", function(){
        var href = jQuery(jQuery(this).find("a")).attr("href");
        location.href = href;
    });
    
    jQuery(document).on("submit", "#event-form", function(e){
        if(jQuery("#donationallowed").length && jQuery("#txtdonationname").length){
          if(jQuery("#donationallowed").val() == 'Yes' && jQuery("#txtdonationname").val() == ""){
            alert("Please enter the name for this event's Charity");
            return false;
          }
        }
        
        jQuery(this).find("input[type='submit']").attr("disabled", "disabled");
        jQuery(this).find(".eventloading").css("display", "block");
    });

   jQuery(document).on("keydown", "input#b_donation_amount", function(e){
      var code = e.which ? e.which : e.keyCode;
      
      if((code >= 48 && code <= 57) || (code >= 96 && code <= 105) || code == 8 || code == 46 || code == 37 || code == 39 || code == 190){
          if(code != 190)
            return true;
          else{
            var count = (jQuery(this).val().match(/\./g) || []).length;
            if(count > 0)
              return false;
            else return true;
          }
      }
      else return false;
   }); 
    
   jQuery(document).on("keyup", "input.ticket_price", function(){
       var value = parseFloat(jQuery(this).val());
       if(value.toString() != "NaN"){
            if(value >= 0 && value <= 25)
              value = value + 1;
            else if(value > 25 && value <= 30)
              value = value + 1.5;
            else if(value > 30 && value <= 40)
              value = value + 2;
            else if(value > 40)
              value = value + 3;
           var label = jQuery(this).parent().find("#label-commission");
           var real = jQuery(this).parent().find("#price-commission");
           if(label.length && real.length){
               jQuery(label).html("Ticket cost with commission will be £"+value);
               real.val(value);
           }
       }
       else{
          var label = jQuery(this).parent().find("#label-commission");
          jQuery(label).html("");
       }
   });

   jQuery(document).on("click", "#tclink", function(e){
      e.preventDefault();
      var tc = jQuery(this).data("tc");
      jQuery("#tccontent").html(jQuery.parseHTML(tc));
      jQuery("#em-termcondition").dialog({
            autoOpen: false,
            height: 300,
            width: 550,
            modal: true,
            buttons: {
                "OK": function(){
                    jQuery("#em-termcondition").dialog( "close" );
                }
            },
            close: function() {
                
            }
        });
        jQuery("#em-termcondition").dialog('open');
   });

   jQuery(document).on("click", "#em-booking-submit-temp", function(e){
      e.preventDefault();
      if(jQuery("#termcondition").length && !jQuery("#termcondition").is(":checked")){
        alert("Please read and agree to the event's terms and conditions");
      }
      else{
        var check = true;
        var count1 = 0;
        var count0 = 0;
        jQuery.each(jQuery("select[id^='em-ticket-spaces-']"), function(id, item){
          if(check){
            if(jQuery(item).val() > 1){
              check = false;
            }
            else if(jQuery(item).val() == 1){
              count1++;
            }
            else if(jQuery(item).val() == 0){
              count0++;
            }
          }
        });

        if(check && count1 == 1){
          jQuery("#em-booking-submit").click();
        }
        else alert("Please select a ticket and you can buy only 1 ticket at a time!");
      }
   });

   jQuery(document).on("change", "select[id^='em-ticket-spaces-']", function(e){
      if(jQuery(this).val() == 1){
        var select = jQuery(this);
        jQuery.each(jQuery("select[id^='em-ticket-spaces-']"), function(id, item){
          if(jQuery(item).attr('id') != select.attr('id')){
            jQuery(item).val(0);
          }
        });
      }
      else{

      }
   });
   
   jQuery(document).on("click", ".sendemails", function(e){
      e.stopPropagation();
       var eventid = jQuery(this).attr("id").replace("sendemail-","");
       
       jQuery("#em-sendemails").dialog({
            autoOpen: false,
            height: 300,
            width: 550,
            modal: true,
            buttons: {
                "Send": function(){
                    var subject = jQuery("#em-sendemails").find("#subject").val();
                    var content = jQuery("#em-sendemails").find("#content").val();
                    var data = {
                        'action': 'send_emails',
                        'eventid': eventid,
                        'subject': subject,
                        'content': content
                    };

                    jQuery.post(ajaxurl, data, function(response) {
                            alert(response.replace("0",""));
                            jQuery("#em-sendemails").dialog( "close" );
                    });
                },
                Cancel: function() {
                    jQuery("#em-sendemails").dialog( "close" );
                }
            },
            close: function() {
                
            }
        });
        jQuery("#em-sendemails").dialog('open');
   });
   
   
});