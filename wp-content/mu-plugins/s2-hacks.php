<?php
//CuongBui
add_action('ws_plugin__s2member_after_configure_user_registration', 's2_redirect_after_registration');
function s2_redirect_after_registration($vars = array()) {
    if (!is_admin() && $vars['processed'] === 'yes') {
	
        $_POST['redirect_to'] = "http://www.racenumber.co.uk/my-account/profile-update/";
    }
}

add_filter('ws_plugin__s2member_add_meta_boxes_excluded_types', 's2_meta_box_excluded_post_types');
function s2_meta_box_excluded_post_types($excluded_types = array(), $vars = array())
	{
		if(!is_super_admin())
			return array_merge($excluded_types, array_keys (get_post_types ()));
		return $excluded_types;
	}
	
add_action("hook_name", "my_action_hook_function");
function my_action_hook_function($vars = array())
	{
		echo "My Hook works.";
 
		# Optional. s2Member® passes you an array of defined variables.
		# print_r($vars); # Would give you a list of defined variables.
		# These are PHP variables defined within the scope of the Hook,
		# at the precise point in which the Hook is fired by s2Member®.
 
		# $vars["__refs"] are also included (with some Hooks).
		# These are internal PHP variable references (very useful).
		# To learn all about references, please see PHP documentation:
		# http://www.php.net/manual/en/language.references.pass.php
	}
?>