<?php

class EM_Gateway_WorldPay extends EM_Gateway {
	//change these properties below if creating a new gateway, not advised to change this for WorldPay
	var $gateway = 'worldpay';
	var $title = 'WorldPay';
	var $status = 4;
	var $status_txt = 'Awaiting WorldPay Payment';
	var $button_enabled = true;
	var $payment_return = true;
	var $count_pending_spaces = false;
	var $supports_multiple_bookings = true;

	/**
	 * Sets up gateaway and adds relevant actions/filters 
	 */
	function __construct() {
		//Booking Interception
	    if( $this->is_active() && absint(get_option('em_'.$this->gateway.'_booking_timeout')) > 0 ){
	        $this->count_pending_spaces = true;
	    }
		parent::__construct();
		$this->status_txt = __('Awaiting WorldPay Payment','em-pro');
		if($this->is_active()) {
			update_option('em_worldpay_option_name', "WorldPay");
			add_action('em_gateway_js', array(&$this,'em_gateway_js'));
			//Gateway-Specific
			add_action('em_template_my_bookings_header',array(&$this,'say_thanks')); //say thanks on my_bookings page
			add_filter('em_bookings_table_booking_actions_4', array(&$this,'bookings_table_actions'),1,2);
			add_filter('em_my_bookings_booking_actions', array(&$this,'em_my_bookings_booking_actions'),1,2);
			//set up cron
			$timestamp = wp_next_scheduled('emp_worldpay_cron');
			if( absint(get_option('em_worldpay_booking_timeout')) > 0 && !$timestamp ){
				$result = wp_schedule_event(time(),'em_minute','emp_worldpay_cron');
			}elseif( !$timestamp ){
				wp_unschedule_event($timestamp, 'emp_worldpay_cron');
			}
		}else{
			//unschedule the cron
			wp_clear_scheduled_hook('emp_worldpay_cron');			
		}
	}
	
	/* 
	 * --------------------------------------------------
	 * Booking Interception - functions that modify booking object behaviour
	 * --------------------------------------------------
	 */
	
	/**
	 * Intercepts return data after a booking has been made and adds worldpay vars, modifies feedback message.
	 * @param array $return
	 * @param EM_Booking $EM_Booking
	 * @return array
	 */
	function booking_form_feedback( $return, $EM_Booking = false ){
		//Double check $EM_Booking is an EM_Booking object and that we have a booking awaiting payment.
		if( is_object($EM_Booking) && $this->uses_gateway($EM_Booking) ){
			if( !empty($return['result']) && $EM_Booking->get_price() > 0 && $EM_Booking->booking_status == $this->status ){
				$return['message'] = get_option('em_worldpay_booking_feedback');	
				$worldpay_url = $this->get_worldpay_url();	
				$worldpay_vars = $this->get_worldpay_vars($EM_Booking);					
				$worldpay_return = array('worldpay_url'=>$worldpay_url, 'worldpay_vars'=>$worldpay_vars);
				$return = array_merge($return, $worldpay_return);
			}else{
				//returning a free message
				$return['message'] = get_option('em_worldpay_booking_feedback_free');
			}
		}
		return $return;
	}
	
	/**
	 * Called if AJAX isn't being used, i.e. a javascript script failed and forms are being reloaded instead.
	 * @param string $feedback
	 * @return string
	 */
	function booking_form_feedback_fallback( $feedback ){
		global $EM_Booking;
		if( is_object($EM_Booking) ){
			$feedback .= "<br />" . __('To finalize your booking, please click the following button to proceed to WorldPay.','em-pro'). $this->em_my_bookings_booking_actions('',$EM_Booking);
		}
		return $feedback;
	}
	
	/**
	 * Triggered by the em_booking_add_yourgateway action, hooked in EM_Gateway. Overrides EM_Gateway to account for non-ajax bookings (i.e. broken JS on site).
	 * @param EM_Event $EM_Event
	 * @param EM_Booking $EM_Booking
	 * @param boolean $post_validation
	 */
	function booking_add($EM_Event, $EM_Booking, $post_validation = false){
		parent::booking_add($EM_Event, $EM_Booking, $post_validation);
		if( !defined('DOING_AJAX') ){ //we aren't doing ajax here, so we should provide a way to edit the $EM_Notices ojbect.
			add_action('option_dbem_booking_feedback', array(&$this, 'booking_form_feedback_fallback'));
		}
	}
	
	/* 
	 * --------------------------------------------------
	 * Booking UI - modifications to booking pages and tables containing worldpay bookings
	 * --------------------------------------------------
	 */
	
	/**
	 * Instead of a simple status string, a resume payment button is added to the status message so user can resume booking from their my-bookings page.
	 * @param string $message
	 * @param EM_Booking $EM_Booking
	 * @return string
	 */
	function em_my_bookings_booking_actions( $message, $EM_Booking){
	    global $wpdb;
		if($this->uses_gateway($EM_Booking) && $EM_Booking->booking_status == $this->status){
		    //first make sure there's no pending payments
		    $pending_payments = $wpdb->get_var('SELECT COUNT(*) FROM '.EM_TRANSACTIONS_TABLE. " WHERE booking_id='{$EM_Booking->booking_id}' AND transaction_gateway='{$this->gateway}' AND transaction_status='Pending'");
		    if( $pending_payments == 0 ){
				//user owes money!
				$worldpay_vars = $this->get_worldpay_vars($EM_Booking);
				$form = '<form action="'.$this->get_worldpay_url().'" method="post">';
				foreach($worldpay_vars as $key=>$value){
					$form .= '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
				}
				$form .= '<input type="submit" value="'.__('Resume Payment','em-pro').'">';
				$form .= '</form>';
				$message .= $form;
		    }
		}
		return $message;		
	}

	/**
	 * Outputs extra custom content e.g. the WorldPay logo by default. 
	 */
	function booking_form(){
		echo get_option('em_'.$this->gateway.'_form');
		echo '<img itemprop="logo" src="http://www.worldpay.com/sites/all/themes/worldpay_subthemev2/logo.png" alt="Worldpay">';
	}
	
	/**
	 * Outputs some JavaScript during the em_gateway_js action, which is run inside a script html tag, located in gateways/gateway.worldpay.js
	 */
	function em_gateway_js(){
		include(dirname(__FILE__).'/gateway.worldpay.js');		
	}
	
	/**
	 * Adds relevant actions to booking shown in the bookings table
	 * @param EM_Booking $EM_Booking
	 */
	function bookings_table_actions( $actions, $EM_Booking ){
		return array(
			'approve' => '<a class="em-bookings-approve em-bookings-approve-offline" href="'.em_add_get_params($_SERVER['REQUEST_URI'], array('action'=>'bookings_approve', 'booking_id'=>$EM_Booking->booking_id)).'">'.__('Approve','dbem').'</a>',
			'delete' => '<span class="trash"><a class="em-bookings-delete" href="'.em_add_get_params($_SERVER['REQUEST_URI'], array('action'=>'bookings_delete', 'booking_id'=>$EM_Booking->booking_id)).'">'.__('Delete','dbem').'</a></span>',
			'edit' => '<a class="em-bookings-edit" href="'.em_add_get_params($EM_Booking->get_event()->get_bookings_url(), array('booking_id'=>$EM_Booking->booking_id, 'em_ajax'=>null, 'em_obj'=>null)).'">'.__('Edit/View','dbem').'</a>',
		);
	}
	
	/*
	 * --------------------------------------------------
	 * WorldPay Functions - functions specific to worldpay payments
	 * --------------------------------------------------
	 */
	
	/**
	 * Retreive the worldpay vars needed to send to the gatway to proceed with payment
	 * @param EM_Booking $EM_Booking
	 */
	function get_worldpay_vars($EM_Booking){
		global $wp_rewrite, $EM_Notices;
		$notify_url = $this->get_payment_return_url();
		$worldpay_vars = array(
			'instId' => get_option('em_'. $this->gateway . "_instId" ), 
			//'cartId' => $EM_Booking->booking_id.'-'.$EM_Booking->event_id,
			'cartId' => $EM_Booking->booking_id,
			'MC_bookingId' => $EM_Booking->booking_id,
			'MC_callback' => $notify_url,
			//'upload' => 1,
			'currency' => get_option('dbem_bookings_currency', 'USD'),
			//'notify_url' =>$notify_url,
			'desc' => "Booking " .$EM_Booking->get_event()->event_name,
			'charset' => 'UTF-8'
		);
		if( get_option('em_'. $this->gateway . "_lc" ) ){
		    $worldpay_vars['lang'] = get_option('em_'. $this->gateway . "_lc" );
		}
		//address fields`and name/email fields to prefill on checkout page (if available)
		$worldpay_vars['email'] = $EM_Booking->get_person()->user_email;
		$worldpay_vars['name'] = $EM_Booking->get_person()->first_name . " " . $EM_Booking->get_person()->last_name;
		if (get_option( 'em_'. $this->gateway . "_status" ) == 'test') {
			$worldpay_vars['name'] ='AUTHORISED';
			$worldpay_vars['testMode'] = '100';
		}		
        if( EM_Gateways::get_customer_field('address', $EM_Booking) != '' ) $worldpay_vars['address'] = EM_Gateways::get_customer_field('address', $EM_Booking);
        if( EM_Gateways::get_customer_field('address_2', $EM_Booking) != '' ) $worldpay_vars['address'] .= " " . EM_Gateways::get_customer_field('address_2', $EM_Booking);
        if( EM_Gateways::get_customer_field('city', $EM_Booking) != '' ) $worldpay_vars['city'] = EM_Gateways::get_customer_field('city', $EM_Booking);
        if( EM_Gateways::get_customer_field('state', $EM_Booking) != '' ) $worldpay_vars['state'] = EM_Gateways::get_customer_field('state', $EM_Booking);
        if( EM_Gateways::get_customer_field('zip', $EM_Booking) != '' ) $worldpay_vars['postcode'] = EM_Gateways::get_customer_field('zip', $EM_Booking);
        if( EM_Gateways::get_customer_field('country', $EM_Booking) != '' ) $worldpay_vars['country'] = EM_Gateways::get_customer_field('country', $EM_Booking);
        
		//tax is added regardless of whether included in ticket price, otherwise we can't calculate post/pre tax discounts
		if( $EM_Booking->get_price_taxes() > 0 ){ 
			$worldpay_vars['tax_cart'] = round($EM_Booking->get_price_taxes(), 2);
		}
		if( get_option('em_'. $this->gateway . "_return" ) != "" ){
			$worldpay_vars['return'] = get_option('em_'. $this->gateway . "_return" );
		}
		if( get_option('em_'. $this->gateway . "_cancel_return" ) != "" ){
			$worldpay_vars['cancel_return'] = get_option('em_'. $this->gateway . "_cancel_return" );
		}

		$count = 1;
		foreach( $EM_Booking->get_tickets_bookings()->tickets_bookings as $EM_Ticket_Booking ){ /* @var $EM_Ticket_Booking EM_Ticket_Booking */
		    //divide price by spaces for per-ticket price
		    //we divide this way rather than by $EM_Ticket because that can be changed by user in future, yet $EM_Ticket_Booking will change if booking itself is saved.
		    $price = $EM_Ticket_Booking->get_price() / $EM_Ticket_Booking->get_spaces();
			if( $price > 0 ){
				//$worldpay_vars['item_name_'.$count] = wp_kses_data($EM_Ticket_Booking->get_ticket()->name);
				//$worldpay_vars['quantity_'.$count] = $EM_Ticket_Booking->get_spaces();
				$worldpay_vars['amount'] += round($price,2);
				$count++;
			}
		}
		//calculate discounts, if any:
		$discount = $EM_Booking->get_price_discounts_amount('pre') + $EM_Booking->get_price_discounts_amount('post');
		if( $discount > 0 ){
			$worldpay_vars['amount'] -= $discount;
		}

		//CuongBui , temporary add for donation, need a better way
		if(isset($EM_Booking->booking_meta['booking']['b_donation_amount']) && is_numeric($EM_Booking->booking_meta['booking']['b_donation_amount'])){
			$worldpay_vars['amount'] += intval($EM_Booking->booking_meta['booking']['b_donation_amount']);
		}

		return apply_filters('em_gateway_worldpay_get_worldpay_vars', $worldpay_vars, $EM_Booking, $this);
	}
	
	/**
	 * gets worldpay gateway url (sandbox or live mode)
	 * @returns string 
	 */
	function get_worldpay_url(){
		return ( get_option('em_'. $this->gateway . "_status" ) == 'test') ? 'https://secure-test.worldpay.com/wcc/purchase':'https://secure.worldpay.com/wcc/purchase';
	}
	
	function say_thanks(){
		if( !empty($_REQUEST['thanks']) ){
			echo "<div class='em-booking-message em-booking-message-success'>".get_option('em_'.$this->gateway.'_booking_feedback_thanks').'</div>';
		}
	}

	/**
	 * Runs when WorldPay sends IPNs to the return URL provided during bookings and EM setup. Bookings are updated and transactions are recorded accordingly. 
	 */
	function handle_payment_return() {
		global $wpdb;
		if( !empty($_POST['transId']) && !empty($_POST['transStatus'])){
			$transaction_id = $_POST['transId'];
			$transaction_status = $_POST['transStatus'];
			$bookingId = $_POST['MC_bookingId'];
			if($transaction_status == "Y"){
	    		$EM_Booking = em_get_booking($bookingId);
	        	if( !empty($EM_Booking->booking_id) ){
	        		$EM_Booking->approve(true, true);
	        		$amount = $amount * -1;
	        		$this->record_transaction($EM_Booking, $amount, 'USD', current_time('mysql'), $transaction_id, __('Refunded','em-pro'), '');
	        		echo "Transaction Processed";
	        		//echo '<meta http-equiv="refresh" content="0; url=http://www.racenumber.co.uk/thankyou">';
	        	}else{
	        		echo "Transaction not found"; //meaningful output
	        	}	        	
			}else{
	        	//Find the transaction and booking, void the transaction, cancel the booking.
	        	$txn = $wpdb->get_row( $wpdb->prepare( "SELECT transaction_id, transaction_gateway_id, transaction_total_amount, booking_id FROM ".EM_TRANSACTIONS_TABLE." WHERE transaction_gateway_id = %s AND transaction_gateway = %s ORDER BY transaction_total_amount DESC LIMIT 1", $transaction_id, $this->gateway ), ARRAY_A );
	        	if(!empty($bookingId) ){
	        		$EM_Booking = em_get_booking($bookingId);
	        		$EM_Booking->cancel();
	        		$wpdb->update(EM_TRANSACTIONS_TABLE, array('transaction_status'=>__('Voided','em-pro'),'transaction_timestamp'=>current_time('mysql')), array('transaction_id'=>$transaction_id));
	        		echo "Transaction Processed";
	        		//echo '<meta http-equiv="refresh" content="0; url=http://www.racenumber.co.uk/cancel">';
	        	}else{
	        		echo "Transaction not found"; //meaningful output
	        	}
			}
		}
	}
	
	
	/*
	 * --------------------------------------------------
	 * Gateway Settings Functions
	 * --------------------------------------------------
	 */
	
	/**
	 * Outputs custom WorldPay setting fields in the settings page 
	 */
	function mysettings() {
		global $EM_options;
		?>
		<table class="form-table">
		<tbody>
		  <tr valign="top">
			  <th scope="row"><?php _e('Success Message', 'em-pro') ?></th>
			  <td>
			  	<input type="text" name="worldpay_booking_feedback" value="<?php esc_attr_e(get_option('em_'. $this->gateway . "_booking_feedback" )); ?>" style='width: 40em;' /><br />
			  	<em><?php _e('The message that is shown to a user when a booking is successful whilst being redirected to WorldPay for payment.','em-pro'); ?></em>
			  </td>
		  </tr>
		  <tr valign="top">
			  <th scope="row"><?php _e('Success Free Message', 'em-pro') ?></th>
			  <td>
			  	<input type="text" name="worldpay_booking_feedback_free" value="<?php esc_attr_e(get_option('em_'. $this->gateway . "_booking_feedback_free" )); ?>" style='width: 40em;' /><br />
			  	<em><?php _e('If some cases if you allow a free ticket (e.g. pay at gate) as well as paid tickets, this message will be shown and the user will not be redirected to WorldPay.','em-pro'); ?></em>
			  </td>
		  </tr>
		  <tr valign="top">
			  <th scope="row"><?php _e('Thank You Message', 'em-pro') ?></th>
			  <td>
			  	<input type="text" name="worldpay_booking_feedback_thanks" value="<?php esc_attr_e(get_option('em_'. $this->gateway . "_booking_feedback_thanks" )); ?>" style='width: 40em;' /><br />
			  	<em><?php _e('If you choose to return users to the default Events Manager thank you page after a user has paid on WorldPay, you can customize the thank you message here.','em-pro'); ?></em>
			  </td>
		  </tr>
		</tbody>
		</table>
		
		<h3><?php echo sprintf(__('%s Options','em-pro'),'WorldPay'); ?></h3>
		<table class="form-table">
		<tbody>
		  <tr valign="top">
			  <th scope="row"><?php _e('WorldPay Merchant\'s own installation Id', 'em-pro') ?></th>
				  <td><input type="text" name="worldpay_instId" value="<?php esc_attr_e( get_option('em_'. $this->gateway . "_instId" )); ?>" />
				  <br />
			  </td>
		  </tr>
		  <tr valign="top">
			  <th scope="row"><?php _e('WorldPay Currency', 'em-pro') ?></th>
			  <td><?php echo esc_html(get_option('dbem_bookings_currency','USD')); ?><br /><i><?php echo sprintf(__('Set your currency in the <a href="%s">settings</a> page.','em-pro'),EM_ADMIN_URL.'&amp;page=events-manager-options#bookings'); ?></i></td>
		  </tr>
		  
		  <tr valign="top">
			  <th scope="row"><?php _e('WorldPay Language', 'em-pro') ?></th>
			  <td>
			  	<select name="worldpay_lc">
			  		<option value=""><?php _e('Default','em-pro'); ?></option>
				  <?php
					$ccodes = em_get_countries();
					$worldpay_lc = get_option('em_'.$this->gateway.'_lc', 'US');
					foreach($ccodes as $key => $value){
						if( $worldpay_lc == $key ){
							echo '<option value="'.$key.'" selected="selected">'.$value.'</option>';
						}else{
							echo '<option value="'.$key.'">'.$value.'</option>';
						}
					}
				  ?>
				  
				  </select>
				  <br />
				  <i><?php _e('WorldPay allows you to select a default language users will see. This is also determined by WorldPay which detects the locale of the users browser. The default would be US.','em-pro') ?></i>
			  </td>
		  </tr>
		  <tr valign="top">
			  <th scope="row"><?php _e('WorldPay Mode', 'em-pro') ?></th>
			  <td>
				  <select name="worldpay_status">
					  <option value="live" <?php if (get_option('em_'. $this->gateway . "_status" ) == 'live') echo 'selected="selected"'; ?>><?php _e('Live Site', 'em-pro') ?></option>
					  <option value="test" <?php if (get_option('em_'. $this->gateway . "_status" ) == 'test') echo 'selected="selected"'; ?>><?php _e('Test Mode (Sandbox)', 'em-pro') ?></option>
				  </select>
				  <br />
			  </td>
		  </tr>
		  <tr valign="top">
			  <th scope="row"><?php _e('Return URL', 'em-pro') ?></th>
			  <td>
			  	<input type="text" name="worldpay_return" value="<?php esc_attr_e(get_option('em_'. $this->gateway . "_return" )); ?>" style='width: 40em;' /><br />
			  </td>
		  </tr>
		  <tr valign="top">
			  <th scope="row"><?php _e('Cancel URL', 'em-pro') ?></th>
			  <td>
			  	<input type="text" name="worldpay_cancel_return" value="<?php esc_attr_e(get_option('em_'. $this->gateway . "_cancel_return" )); ?>" style='width: 40em;' /><br />
			  	<em><?php _e('Whilst paying on WorldPay, if a user cancels, they will be redirected to this page.', 'em-pro'); ?></em>
			  </td>
		  </tr>

		  <tr valign="top">
			  <th scope="row"><?php _e('Delete Bookings Pending Payment', 'em-pro') ?></th>
			  <td>
			  	<input type="text" name="worldpay_booking_timeout" style="width:50px;" value="<?php esc_attr_e(get_option('em_'. $this->gateway . "_booking_timeout" )); ?>" style='width: 40em;' /> <?php _e('minutes','em-pro'); ?><br />
			  	<em><?php _e('Once a booking is started and the user is taken to WorldPay, Events Manager stores a booking record in the database to identify the incoming payment. These spaces may be considered reserved if you enable <em>Reserved unconfirmed spaces?</em> in your Events &gt; Settings page. If you would like these bookings to expire after x minutes, please enter a value above (note that bookings will be deleted, and any late payments will need to be refunded manually via WorldPay).','em-pro'); ?></em>
			  </td>
		  </tr>
		  <tr valign="top">
			  <th scope="row"><?php _e('Manually approve completed transactions?', 'em-pro') ?></th>
			  <td>
			  	<input type="checkbox" name="worldpay_manual_approval" value="1" <?php echo (get_option('em_'. $this->gateway . "_manual_approval" )) ? 'checked="checked"':''; ?> /><br />
			  	<em><?php _e('By default, when someone pays for a booking, it gets automatically approved once the payment is confirmed. If you would like to manually verify and approve bookings, tick this box.','em-pro'); ?></em><br />
			  	<em><?php echo sprintf(__('Approvals must also be required for all bookings in your <a href="%s">settings</a> for this to work properly.','em-pro'),EM_ADMIN_URL.'&amp;page=events-manager-options'); ?></em>
			  </td>
		  </tr>
		</tbody>
		</table>
		<?php
	}

	/* 
	 * Run when saving WorldPay settings, saves the settings available in EM_Gateway_WorldPay::mysettings()
	 */
	function update() {
		parent::update();
		$gateway_options = array(
			$this->gateway . "_instId" => $_REQUEST[ $this->gateway.'_instId' ],
			$this->gateway . "_site" => $_REQUEST[ $this->gateway.'_site' ],
			$this->gateway . "_currency" => $_REQUEST[ 'currency' ],
			$this->gateway . "_lc" => $_REQUEST[ $this->gateway.'_lc' ],
			$this->gateway . "_status" => $_REQUEST[ $this->gateway.'_status' ],
			$this->gateway . "_tax" => $_REQUEST[ $this->gateway.'_button' ],
			// $this->gateway . "_format_logo" => $_REQUEST[ $this->gateway.'_format_logo' ],
			// $this->gateway . "_format_border" => $_REQUEST[ $this->gateway.'_format_border' ],
			$this->gateway . "_manual_approval" => $_REQUEST[ $this->gateway.'_manual_approval' ],
			$this->gateway . "_booking_feedback" => wp_kses_data($_REQUEST[ $this->gateway.'_booking_feedback' ]),
			$this->gateway . "_booking_feedback_free" => wp_kses_data($_REQUEST[ $this->gateway.'_booking_feedback_free' ]),
			$this->gateway . "_booking_feedback_thanks" => wp_kses_data($_REQUEST[ $this->gateway.'_booking_feedback_thanks' ]),
			$this->gateway . "_booking_timeout" => $_REQUEST[ $this->gateway.'_booking_timeout' ],
			$this->gateway . "_return" => $_REQUEST[ $this->gateway.'_return' ],
			$this->gateway . "_cancel_return" => $_REQUEST[ $this->gateway.'_cancel_return' ],
			$this->gateway . "_form" => $_REQUEST[ $this->gateway.'_form' ]
		);
		foreach($gateway_options as $key=>$option){
			update_option('em_'.$key, stripslashes($option));
		}
		//default action is to return true
		return true;

	}
}
EM_Gateways::register_gateway('worldpay', 'EM_Gateway_WorldPay');

/**
 * Deletes bookings pending payment that are more than x minutes old, defined by worldpay options. 
 */
function em_gateway_worldpay_booking_timeout(){
	global $wpdb;
	//Get a time from when to delete
	$minutes_to_subtract = absint(get_option('em_worldpay_booking_timeout'));
	if( $minutes_to_subtract > 0 ){
		//get booking IDs without pending transactions
		$cut_off_time = date('Y-m-d H:i:s', current_time('timestamp') - ($minutes_to_subtract * 60));
		$booking_ids = $wpdb->get_col('SELECT b.booking_id FROM '.EM_BOOKINGS_TABLE.' b LEFT JOIN '.EM_TRANSACTIONS_TABLE." t ON t.booking_id=b.booking_id  WHERE booking_date < '{$cut_off_time}' AND booking_status=4 AND transaction_id IS NULL" );
		if( count($booking_ids) > 0 ){
			//first delete ticket_bookings with expired bookings
			$sql = "DELETE FROM ".EM_TICKETS_BOOKINGS_TABLE." WHERE booking_id IN (".implode(',',$booking_ids).");";
			$wpdb->query($sql);
			//then delete the bookings themselves
			$sql = "DELETE FROM ".EM_BOOKINGS_TABLE." WHERE booking_id IN (".implode(',',$booking_ids).");";
			$wpdb->query($sql);
		}
	}
}
add_action('emp_worldpay_cron', 'em_gateway_worldpay_booking_timeout');
?>