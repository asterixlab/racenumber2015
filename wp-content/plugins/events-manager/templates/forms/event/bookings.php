<?php
global $EM_Event, $post, $allowedposttags, $EM_Ticket, $col_count, $wpdb;

//Get EventForm
$formData =  json_decode(stripslashes($wpdb->get_var($wpdb->prepare( 
	"
		SELECT form_data 
		FROM `wp_event_form` 
		WHERE event_id = %d
	", 
	$EM_Event->event_id
))));
?>
<div id="event-rsvp-box">
	<input id="event-rsvp" name='event_rsvp' value='1' type='checkbox' <?php echo ($EM_Event->event_rsvp) ? 'checked="checked"' : ''; ?> />
	&nbsp;&nbsp;
	<?php _e ( 'Enable registration for this event', 'dbem' )?>
</div>
<div id="event-rsvp-options" style="<?php echo ($EM_Event->event_rsvp) ? '':'display:none;' ?>">
	<?php do_action('em_events_admin_bookings_header', $EM_Event); ?>
	<div id="em-tickets-form">
	<?php
	//get tickets here and if there are none, create a blank ticket
	$EM_Tickets = $EM_Event->get_tickets();
	if( count($EM_Tickets->tickets) == 0 ){
		$EM_Tickets->tickets[] = new EM_Ticket();
		$delete_temp_ticket = true;
	}
	if( get_option('dbem_bookings_tickets_single') && count($EM_Tickets->tickets) == 1 ){
		?>
		<h4><?php esc_html_e('Ticket Options','dbem'); ?></h4>
		<?php
		$col_count = 1;	
		$EM_Ticket = $EM_Tickets->get_first();				
		include( em_locate_template('forms/ticket-form.php') ); //in future we'll be accessing forms/event/bookings-ticket-form.php directly
	}else{
		?>
		<!-- CuongBui -->
		<h4><?php esc_html_e('Entry Options','dbem'); ?></h4>
		<!-- <h4><?php //esc_html_e('Tickets','dbem'); ?></h4> -->
		<p><em><?php esc_html_e('Here you can set multiple entry options where different costs are displayed to the user. E.g. Attached or Unattached entry options can be created. You can also set a discounted "Early Bird" price which expires on a specific date and time. If you have any question about this, just drop us an email.', 'dbem'); ?></em></p>					
		<!-- <p><em><?php //esc_html_e('You can have single or multiple tickets, where certain tickets become availalble under certain conditions, e.g. early bookings, group discounts, maximum bookings per ticket, etc.', 'dbem'); ?> <?php //esc_html_e('Basic HTML is allowed in ticket labels and descriptions.','dbem'); ?></em></p>					 -->
		<table class="form-table">
			<thead>
				<tr valign="top">
					<th colspan="2"><?php esc_html_e('Option Name','dbem'); ?></th>
					<th><?php esc_html_e('Price','dbem'); ?></th>
					<th><?php esc_html_e('Min/Max','dbem'); ?></th>
					<th><?php esc_html_e('Start/End','dbem'); ?></th>
					<th><?php esc_html_e('Avail. Spaces','dbem'); ?></th>
					<th><?php esc_html_e('Booked Spaces','dbem'); ?></th>
					<th>&nbsp;</th>
				</tr>
			</thead>    
			<tfoot>
				<tr valign="top">
					<td colspan="8">
						<a href="#" id="em-tickets-add"><?php esc_html_e('Add new option','dbem'); ?></a>
					</td>
				</tr>
			</tfoot>
			<?php
				$EM_Ticket = new EM_Ticket();
				$EM_Ticket->event_id = $EM_Event->event_id;
				array_unshift($EM_Tickets->tickets, $EM_Ticket); //prepend template ticket for JS
				$col_count = 0;
				foreach( $EM_Tickets->tickets as $EM_Ticket){
					/* @var $EM_Ticket EM_Ticket */
					?>
					<tbody id="em-ticket-<?php echo $col_count ?>" <?php if( $col_count == 0 ) echo 'style="display:none;"' ?>>
						<tr class="em-tickets-row">
							<td class="ticket-status"><span class="<?php if($EM_Ticket->ticket_id && $EM_Ticket->is_available()){ echo 'ticket_on'; }elseif($EM_Ticket->ticket_id > 0){ echo 'ticket_off'; }else{ echo 'ticket_new'; } ?>"></span></td>													
							<td class="ticket-name">
								<span class="ticket_name"><?php if($EM_Ticket->ticket_members) echo '* ';?><?php echo wp_kses_data($EM_Ticket->ticket_name); ?></span>
								<div class="ticket_description"><?php echo wp_kses($EM_Ticket->ticket_description,$allowedposttags); ?></div>
								<div class="ticket-actions">
									<a href="#" class="ticket-actions-edit"><?php esc_html_e('Edit','dbem'); ?></a> 
									<?php if( count($EM_Ticket->get_bookings()->bookings) == 0 ): ?>
									| <a href="<?php bloginfo('wpurl'); ?>/wp-load.php" class="ticket-actions-delete"><?php esc_html_e('Delete','dbem'); ?></a>
									<?php else: ?>
									| <a href="<?php echo EM_ADMIN_URL; ?>&amp;page=events-manager-bookings&ticket_id=<?php echo $EM_Ticket->ticket_id ?>"><?php esc_html_e('View Bookings','dbem'); ?></a>
									<?php endif; ?>
								</div>
							</td>
							<td class="ticket-price">
								<span class="ticket_price"><?php echo ($EM_Ticket->ticket_price) ? esc_html($EM_Ticket->ticket_price) : esc_html__('Free','dbem'); ?></span>
							</td>
							<td class="ticket-limit">
								<span class="ticket_min">
									<?php  echo ( !empty($EM_Ticket->ticket_min) ) ? esc_html($EM_Ticket->ticket_min):'-'; ?>
								</span> / 
								<span class="ticket_max"><?php echo ( !empty($EM_Ticket->ticket_max) ) ? esc_html($EM_Ticket->ticket_max):'-'; ?></span>
							</td>
							<td class="ticket-time">
								<span class="ticket_start"><?php echo ( !empty($EM_Ticket->ticket_start) ) ? date(get_option('dbem_date_format'), $EM_Ticket->start_timestamp):''; ?></span>
								<span class="ticket_start_time"><?php echo ( !empty($EM_Ticket->ticket_start) ) ? date( em_get_hour_format(), $EM_Ticket->start_timestamp):''; ?></span>
								<br />
								<span class="ticket_end"><?php echo ( !empty($EM_Ticket->ticket_end) ) ? date(get_option('dbem_date_format'), $EM_Ticket->end_timestamp):''; ?></span>
								<span class="ticket_end_time"><?php echo ( !empty($EM_Ticket->ticket_end) ) ? date( em_get_hour_format(), $EM_Ticket->end_timestamp):''; ?></span>
							</td>
							<td class="ticket-qty">
								<span class="ticket_available_spaces"><?php echo $EM_Ticket->get_available_spaces(); ?></span>/
								<span class="ticket_spaces"><?php echo $EM_Ticket->get_spaces() ? $EM_Ticket->get_spaces() : '-'; ?></span>
							</td>
							<td class="ticket-booked-spaces">
								<span class="ticket_booked_spaces"><?php echo $EM_Ticket->get_booked_spaces(); ?></span>
							</td>
							<?php do_action('em_event_edit_ticket_td', $EM_Ticket); ?>
						</tr>
						<tr class="em-tickets-row-form" style="display:none;">
							<td colspan="<?php echo apply_filters('em_event_edit_ticket_td_colspan', 7); ?>">
								<?php include( em_locate_template('forms/event/bookings-ticket-form.php')); ?>
								<div class="em-ticket-form-actions">
								<button type="button" class="ticket-actions-edited"><?php esc_html_e('Close Ticket Editor','dbem')?></button>
								</div>
							</td>
						</tr>
					</tbody>
					<?php
					$col_count++;
				}
				array_shift($EM_Tickets->tickets);
			?>
		</table>
	<?php 
	}
	?>
	</div>
	<div id="em-booking-options">
	<?php if( !get_option('dbem_bookings_tickets_single') || count($EM_Ticket->get_event()->get_tickets()->tickets) > 1 ): ?>
	<h4><?php esc_html_e('Event Options','dbem'); ?></h4>
	<?php 
		//CuongBui, temporary for donation feature
		$attributes = em_get_attributes();
		if( count( $attributes['names'] ) > 0 && in_array("e_donation_allowed", $attributes['names']) && in_array("e_donation_name", $attributes['names'])){
	?>
	<p>
		<label><?php esc_html_e('Donation Options','dbem'); ?></label>
		<div class="event-attributes">
			<label class="donationlabel" for="em_attributes[e_donation_allowed]">Donation Allowed</label><br/>
			<select id="donationallowed" style="width: 150px; height: 30px;" name="em_attributes[e_donation_allowed]">

				<?php foreach($attributes['values']['e_donation_allowed'] as $attribute_val): ?>

					<?php if( is_array($EM_Event->event_attributes) && array_key_exists('e_donation_allowed', $EM_Event->event_attributes) && $EM_Event->event_attributes['e_donation_allowed']==$attribute_val ): ?>

						<option selected="selected"><?php echo $attribute_val; ?></option>

					<?php else: ?>

						<option><?php echo $attribute_val; ?></option>

					<?php endif; ?>

				<?php endforeach; ?>

			</select>
			<br/>
			<div id="donationname">
				<label class="donationlabel" for="em_attributes[e_donation_name]">Donation Name</label><br/>
				<?php $default_value = (!empty($attributes['values']['e_donation_name'][0])) ? $attributes['values']['e_donation_name'][0]:''; ?>
				<input type="text" id="txtdonationname" name="em_attributes[e_donation_name]" value="<?php echo array_key_exists('e_donation_name', $EM_Event->event_attributes) ? esc_attr($EM_Event->event_attributes['e_donation_name'], ENT_QUOTES):''; ?>" value="<?php echo $default_value; ?>" />
				<input type="hidden" name="em_attributes[e_event_date_various]" value="<?php echo $EM_Event->event_attributes['e_event_date_various'] ?>" id="txt_e_event_date_various">
			</div>
		</div>
	</p>
	<?php } ?>


	<div class="custom-fields">
		<?php 
		//TuTV Add custom fields
			wp_enqueue_script( 'libfb-script', plugins_url( '../../../includes/js/formbuilder/libs.min.js', __FILE__ ) );
			wp_enqueue_script( 'formbuilder-script', plugins_url( '../../../includes/js/formbuilder/formbuilder.js', __FILE__ ) );
			wp_enqueue_style( 'formbuilder-style', plugins_url( '../../../includes/css/formbuilder.css', __FILE__ ), array(), '20120208', 'all' );
		?>	
		<script>
			jQuery(document).ready(function( $ ) {

			        var myForm = new formbuilder({

			            targets: jQuery('.formbuilder'),
			            // A callback allowing you to handle saving the form
			            save: function(formData) {
			            	console.log(JSON.stringify(formData));
			                // $.ajax({
			                //     url: this._opts.save_url,
			                //     data: JSON.stringify(formData),
			                //     contentType: "application/json",
			                //     type: "post",
			                //     success: function() {

			                //     }
			                // })
			            },
			            form_id: 'eoptions',
			            templateBasePath: '<?php echo plugins_url( "../../../includes/templates/builder", __FILE__ ) ?>'
			            <?php if($formData != null && json_encode($formData->model) !== ""){ ?>,
			            startingModel: JSON.parse(<?php echo "'".json_encode($formData->model)."'";?>)
			            <?php } ?>
			        });

			        $('#event-form').submit(function() {					        
					    	var formData = myForm.getData();
					    	$("#custom-fields-data").val(JSON.stringify(formData));
					});

			});
		</script>		
		<label><?php esc_html_e('Custom fields Options','dbem'); ?></label>
		<div class="formbuilder"></div>
		<input type="hidden" name="custom-fields-data" id="custom-fields-data" value="" />
	</div>

	<p>
		<label><?php esc_html_e('Maximum Number of Entries Available','dbem'); ?></label>
		<!-- <label><?php //esc_html_e('Total Spaces','dbem'); ?></label> -->
		<input type="text" name="event_spaces" value="<?php if( $EM_Event->event_spaces > 0 ){ echo $EM_Event->event_spaces; } ?>" /><br />
		<em><?php esc_html_e('Individual entries with remaining spaces will not be available if total booking spaces reach this limit. Leave blank for no limit.','dbem'); ?></em>
	</p>
	<!-- CuongBui -->
	<!-- <p>
		<label><?php esc_html_e('Maximum Spaces Per Booking','dbem'); ?></label>
		<input type="text" name="event_rsvp_spaces" value="<?php if( $EM_Event->event_rsvp_spaces > 0 ){ echo $EM_Event->event_rsvp_spaces; } ?>" /><br />
		<em><?php esc_html_e('If set, the total number of spaces for a single booking to this event cannot exceed this amount.','dbem'); ?><?php esc_html_e('Leave blank for no limit.','dbem'); ?></em>
	</p> -->
	<?php if( !$EM_Event->is_recurring() ): ?>
	<p>
		<label><?php esc_html_e('Entry Closing Date','dbem'); ?></label>
		<!-- <label><?php //esc_html_e('Booking Cut-Off Date','dbem'); ?></label> -->
		<span class="em-date-single">
			<input id="em-bookings-date-loc" class="em-date-input-loc" type="text" />
			<input id="em-bookings-date" class="em-date-input" type="hidden" name="event_rsvp_date" value="<?php echo $EM_Event->event_rsvp_date; ?>" />
		</span>
		<input type="text" name="event_rsvp_time" class="em-time-input" maxlength="8" size="8" value="<?php echo date( em_get_hour_format(), $EM_Event->rsvp_end ); ?>">
		<br />
		<em><?php esc_html_e('This is the definite date after which bookings will be closed for this event, regardless of individual ticket settings above. Default value will be the event start date.','dbem'); ?></em>
	</p>
	<?php endif; ?>
	<?php endif; ?>
	</div>
	<?php
		if( !empty($delete_temp_ticket) ){
			array_pop($EM_Tickets->tickets);
		}
		do_action('em_events_admin_bookings_footer', $EM_Event); 
	?>
</div>